//=============================================================================================================
#include "Required.h"
//=============================================================================================================
#include "Cvars.h"
#include "Defines.h"
#include "DHExecutable.h"
#include "Init.h"
#include "Globals.h"
#include "Hack.h"
#include "LDE.h"
#include "Logic.h"
#include "Memory.h"
//=============================================================================================================
uintptr_t GetResourceOffset ( const char* InitString )
{
	std::map< unsigned int, unsigned int >::iterator MapIterator;

	MapIterator = g_MapOffsets.find ( HashString ( InitString ) );

	if ( MapIterator != g_MapOffsets.end() )
	{
		return MapIterator->second;
	}

	return 0;
}
//=============================================================================================================
uintptr_t GetOffsetSize ( uintptr_t Type )
{
	switch ( Type )
	{
		case TYPE_BOOL:
		{
			return 1;
		}
		break;

		case TYPE_CHAR:
		{
			return 1;
		}
		break;

		case TYPE_SHORT:
		{
			return 2;
		}
		break;

		case TYPE_FLOAT:
		{
			return 4;
		}
		break;

		case TYPE_LONG:
		{
			return 4;
		}
		break;

		case TYPE_DOUBLE:
		{
			return 8;
		}
		break;
	}

	return 0;
}
//=============================================================================================================
void UpdateCvars()
{	
	bool GotValue = false, Set = false;
	
	static bool NotFirstRun = false;

	DWORD ThreadIDChild = 0;

	int ReturnCode = 0, ValueInt = 0;

	float Value = 0, ValueFloat = 0;

	static HWND LastWindowFocus;

	GUITHREADINFO info = {0};

	static POINT point = {0};

	static RECT rect = {0};
	
	uintptr_t Offset = 0, OffsetSize = 0;

	info.cbSize = sizeof ( GUITHREADINFO );

	static bool Attached = false;
	
	char FormattedString[ 256 ];
	char WindowTextString[ 256 ];

	if ( g_Memory.Read ( ( LPCVOID )( g_GameProcessBase + DH_GlobalPlayerPointer_RVADisplacement ), sizeof ( uintptr_t ), &g_PlayerBase ) )
	{	
		if ( Attached == false )
		{
			for ( uintptr_t TabIterator = 0; TabIterator < END_TAB_ENUM; TabIterator++ )
			{
				for ( uintptr_t TabSubIterator = 0; TabSubIterator < g_Tabs[ TabIterator ].m_ChildWindows.size(); TabSubIterator++ )
				{
					ThreadIDChild = GetWindowThreadProcessId ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Window, NULL );

					AttachThreadInput ( ThreadIDChild, GetCurrentThreadId(), TRUE );

					g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Active = NOT_ACTIVE;
				}
			}

			Attached = true;
		}
		
		for ( uintptr_t TabIterator = 0; TabIterator < END_TAB_ENUM; TabIterator++ )
		{
			for ( uintptr_t TabSubIterator = 0; TabSubIterator < g_Tabs[ TabIterator ].m_ChildWindows.size(); TabSubIterator++ )
			{			
				if ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Type != 0 )
				{
					switch ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Type )
					{
						case EDITBOX:
						{					
							ThreadIDChild = GetWindowThreadProcessId ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Window, NULL );

							Offset = GetResourceOffset ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetIniString() );

							OffsetSize = GetOffsetSize ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetType() );

							if ( GetGUIThreadInfo ( ThreadIDChild, &info ) )
							{
								GotValue = false;
								Value = ValueFloat = 0;
								ValueInt = 0;

								if ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetType() == TYPE_FLOAT )
								{
									if ( g_Memory.Read ( ( LPCVOID )( g_PlayerBase + Offset ), OffsetSize, &ValueFloat ) )
									{
										GotValue = true;

										Value = ValueFloat;
									}
								}
								else
								{
									if ( g_Memory.Read ( ( LPCVOID )( g_PlayerBase + Offset ), OffsetSize, &ValueInt ) )
									{
										GotValue = true;

										Value = ( float )ValueInt;
									}
								}

								if ( GotValue )
								{
									GetCursorPos ( &point );
									GetWindowRect ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Window, &rect );
									
									if ( info.hwndFocus == g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Window
										&& info.hwndFocus == LastWindowFocus )
									{										
										if ( GetKeyState ( VK_RETURN ) & 0x8000
											|| ( GetKeyState ( VK_LBUTTON ) & 0x8000
												&& point.x > rect.right || point.x < rect.left
												|| point.y < rect.top || point.y > rect.bottom ) )
										{
											SetFocus ( g_Tabs[ TabIterator ].m_Window );

											g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Active |= ACTIVE_FINISHED;
										}
										else
										{
											if ( GetWindowText ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Window, 
												WindowTextString, sizeof ( WindowTextString ) ) )
											{
												switch ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetType() )
												{
													case TYPE_FLOAT:
													{
														ValueFloat = atof ( WindowTextString );
														
														if ( ValueFloat != 0 )
														{																										
															g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->SetValue ( ValueFloat );
														}
													}
													break;

													default:
													{
														ValueInt = atoi ( WindowTextString );

														if ( ValueInt != 0 )
														{
															g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->SetValue ( ( float )ValueInt );
														}
													}
												}

												g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Active |= ACTIVE_IN_EDITBOX;
											}
											else
											{
												switch ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetType() )
												{
													case TYPE_FLOAT:
													{
														ReturnCode = sprintf_s ( FormattedString, "%f", ValueFloat );
													}
													break;

													default:
													{
														ReturnCode = sprintf_s ( FormattedString, "%i", ValueInt );
													}
													break;
												}
										
												if ( ReturnCode >= 0 )
												{
													SendMessage ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Window, WM_SETTEXT, NULL, ( LPARAM )FormattedString );
												}
											}
										}
									}
									else
									{	
										if ( Value != g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetValue() 
											|| NotFirstRun == false )
										{
											switch ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetType() )
											{
												case TYPE_FLOAT:
												{
													ReturnCode = sprintf_s ( FormattedString, "%f", ValueFloat );
												}
												break;

												default:
												{
													ReturnCode = sprintf_s ( FormattedString, "%i", ValueInt );
												}
												break;
											}
											
											if ( ReturnCode >= 0 )
											{
												SendMessage ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Window, WM_SETTEXT, NULL, ( LPARAM )FormattedString );
											}
										}
									}

									LastWindowFocus = info.hwndFocus;

									if ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Active == NOT_ACTIVE )
									{
										g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->SetValue ( Value );
									}
								}
							}
						}
						break;

						case CHECKBOX:
						{	
							if ( SendMessage ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Window, BM_GETCHECK, 0, 0 ) == BST_CHECKED )
							{									
								g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->SetValue ( 1 );
							}
							else
							{
								g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->SetValue ( 0 );
							}
						}
						break;
					}
				}
			}
		}
	}

	if ( NotFirstRun == false )
	{
		NotFirstRun = true;
	}
}
//=============================================================================================================
void RunHacks ( void )
{
	int ValueInt = 0;

	float ValueFloat = 0;

	uintptr_t Offset = 0, OffsetSize = 0, Type;

	for ( uintptr_t TabIterator = 0; TabIterator < END_TAB_ENUM; TabIterator++ )
	{
		for ( uintptr_t TabSubIterator = 0; TabSubIterator < g_Tabs[ TabIterator ].m_ChildWindows.size(); TabSubIterator++ )
		{
			Offset = GetResourceOffset ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetIniString() );

			OffsetSize = GetOffsetSize ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetType() );

			if ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Type != 0 )
			{
				switch ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Type )
				{
					case EDITBOX:
					{
						if ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Active & ACTIVE_IN_EDITBOX  
							&& g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Active & ACTIVE_FINISHED
							&& Offset != 0 )
						{
							switch ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetType() )
							{
								case TYPE_FLOAT:
								{
									ValueFloat = g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetValue();

									g_Memory.Write ( ( LPVOID )( g_PlayerBase + Offset ), OffsetSize, &ValueFloat );
								}
								break;

								default:
								{
									ValueInt = ( int )g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetValue();

									g_Memory.Write ( ( LPVOID )( g_PlayerBase + Offset ), OffsetSize, &ValueInt );
								}
								break;
							}


							g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_Active = NOT_ACTIVE;
						}
					}
					break;

					case CHECKBOX:
					{							
						if ( Offset != 0 
							&& g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetValue() )
						{
							ValueFloat = g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetValue();
							ValueInt = ( int )g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetValue();

							Type = g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetType();

							if ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_SubType & CHECKBOX_MAX 
								&& g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetParent() )
							{
								OffsetSize = GetOffsetSize ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetParent()->GetType() );
								ValueFloat = g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetParent()->GetMaximum();
								ValueInt = ( int )g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetParent()->GetMaximum();

								Type = g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetParent()->GetType();
							}
							if ( g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_SubType &  CHECKBOX_ENABLED )
							{
								ValueFloat = g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetValue();
								ValueInt = ( int )g_Tabs[ TabIterator ].m_ChildWindows[ TabSubIterator ].m_CVar->GetValue();
							}

							switch ( Type )
							{
								case TYPE_FLOAT:
								{									
									g_Memory.Write ( ( LPVOID )( g_PlayerBase + Offset ), OffsetSize, &ValueFloat );
								}
								break;

								default:
								{
									g_Memory.Write ( ( LPVOID )( g_PlayerBase + Offset ), OffsetSize, &ValueInt );
								}
								break;
							}
						}
					}									
					break;
				}
			}
		}
	}
}
//=============================================================================================================