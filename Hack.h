//=============================================================================================================
extern HANDLE g_ProcessGame;
extern uintptr_t g_ProcessID;
extern uintptr_t g_GameProcessBase;
extern uintptr_t g_GameProcessSize;
extern uintptr_t g_GameProcessCodeSectionSize;
//=============================================================================================================
DWORD HackThread ( LPVOID lpArgs );
//=============================================================================================================