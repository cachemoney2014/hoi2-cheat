//=============================================================================================================
#include "Required.h"
//=============================================================================================================
#include "Cvars.h"
#include "Defines.h"
#include "Init.h"
#include "Globals.h"
//=============================================================================================================
Resources_t g_Resources;
//=============================================================================================================
Desktop_t g_Desktop;
DWORD g_ThreadID;
HANDLE g_ThreadHandle;
HWND g_TabControl;
HWND g_SubTabControl;
HWND g_MainWindow;
//=============================================================================================================
uintptr_t g_PlayerBase;
//=============================================================================================================
int g_GameID;
//=============================================================================================================
int g_WindowX = 0, g_WindowY = 0;
int g_WindowSizeWidth = 0, g_WindowHeight = 0;
int g_TabItem = 0;
//=============================================================================================================
char* g_ResourceNames[ RESOURCE_ARRAY_SIZE ] = 
{
	"Oil",
	"Metal",
	"Energy",
	"Rares",
	"Supplies",
	"Money",
	"Manpower",
	"Nukes",
	"IC",
	"Total IC"
};
//=============================================================================================================
CVar_Base* CheckBoxFreezeResources[ RESOURCE_ARRAY_SIZE ] =
{
	new CVar ( "Freeze Oil", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Freeze Metal", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Freeze Energy", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Freeze Rares", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Freeze Supplies", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Freeze Money", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Freeze Manpower", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Freeze Nukes", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Freeze BaseIC", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Freeze TotalIC", 0, 1, 0, TYPE_BOOL )
};
//=============================================================================================================
CVar_Base* CheckBoxMaxResources[ RESOURCE_ARRAY_SIZE ] =
{
	new CVar ( "Max Oil", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Metal", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Energy", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Rares", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Supplies", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Money", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Manpower", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Nukes", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max BaseIC", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max TotalIC", 0, 1, 0, TYPE_BOOL )
};
//=============================================================================================================
CVar_Base* EditBoxResources[ RESOURCE_ARRAY_SIZE ] =
{
	new CVar ( "Editbox Oil", 0, 999999999, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Metal", 0, 999999999, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Energy", 0, 999999999, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Rares", 0, 999999999, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Supplies", 0, 999999999, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Money", 0, 999999999, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Manpower", 0, 999999999, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Nukes", 0, LONG_MAX, 0, TYPE_LONG ),
	new CVar ( "Editbox BaseIC", 0, LONG_MAX, 0, TYPE_LONG ),
	new CVar ( "Editbox TotalIC", 0, LONG_MAX, 0, TYPE_LONG )
};
//=============================================================================================================
char* g_MissionNames[ MISSION_ARRAY_SIZE ] =
{
	"Attack",
	"Rebase",
	"Strategic Redeployment",
	"Support Attack",
	"Support Defense",
	"Reserves",
	"Anti-Partisan Duty",
	"Planned Defense",
	"Air Superiority",
	"Ground Attack",
	"Ground Support",
	"Strategic Bombardment",
	"Logistical Strike",
	"Runway Cratering",
	"Installation Strike",
	"Naval Strike",
	"Port Strike",
	"Bomb Convoys",
	"Airsupply",
	"Airborne Assault",
	"Nuke",
	"Air Scramble",
	"Convoy Raiding",
	"ASW",
	"Naval Interdiction",
	"Shore Bombardment",
	"Amphibious Assault",
	"Sea Transport",
	"Naval Combat Patrol",
	"Carrier Strike on Port",
	"Carrier Strike on Airbase",
	"Sneak Move",
	"Naval Scramble",
	"Amphibious Size"
};
//=============================================================================================================
CVar_Base* CheckBoxEnableMissions[ MISSION_ARRAY_SIZE ] =
{
	new CVar ( "Enable Attack", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Rebase", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Strategic Redeployment", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Support Attack", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Support Defense", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Reserves", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Anti-Partisan Duty", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Planned Defense", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Air Superiority", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Ground Attack", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Ground Support", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Strategic Bombardment", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Logistical Strike", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Runway Cratering", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Installation Strike", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Naval Strike", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Port Strike", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Bomb Convoys", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Airsupply", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Airborne Assault", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Nuke", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Air Scramble", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Convoy Raiding", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable ASW", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Naval Interdiction", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Shore Bombardment", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Amphibious Assault", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Sea Transport", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Naval Combat Patrol", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Carrier Strike on Port", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Carrier Strike on Airbase", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Sneak Move", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Naval Scramble", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Enable Amphibious Size", 0, 1, 0, TYPE_BOOL )
};
//=============================================================================================================
CVar_Base* CheckBoxMaxMissions[ MISSION_ARRAY_SIZE ] =
{
	new CVar ( "Max Attack", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Rebase", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Strategic Redeployment", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Support Attack", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Support Defense", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Reserves", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Anti-Partisan Duty", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Planned Defense", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Air Superiority", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Ground Attack", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Ground Support", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Strategic Bombardment", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Logistical Strike", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Runway Cratering", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Installation Strike", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Naval Strike", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Port Strike", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Bomb Convoys", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Airsupply", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Airborne Assault", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Nuke", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Air Scramble", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Convoy Raiding", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max ASW", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Naval Interdiction", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Shore Bombardment", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Amphibious Assault", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Sea Transport", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Naval Combat Patrol", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Carrier Strike on Port", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Carrier Strike on Airbase", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Sneak Move", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Naval Scramble", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Amphibious Size", 0, 1, 0, TYPE_BOOL )
};
//=============================================================================================================
CVar_Base* EditBoxMissions[ MISSION_ARRAY_SIZE ] =
{
	new CVar ( "Editbox Attack", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Rebase", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Strategic Redeployment", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Support Attack", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Support Defense", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Reserves", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Anti-Partisan Duty", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Planned Defense", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Air Superiority", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Ground Attack", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Ground Support", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Strategic Bombardment", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Logistical Strike", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Runway Cratering", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Installation Strike", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Naval Strike", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Port Strike", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Bomb Convoys", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Air Supply", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Airborne Assault", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Nuke", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Air Scramble", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Convoy Raiding", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox ASW", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Naval Interdiction", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Shore Bombardment", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Amphibious Assault", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Sea Transport", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Naval Combat Patrol", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Carrier Strike on Port", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Carrier Strike on Airbase", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Sneak Move", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Naval Scramble", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Max Amphibious Size", 0, LONG_MAX, 0, TYPE_LONG ),
};
//=============================================================================================================
char* g_ProductionAndSupplies[ PRODUCTION_AND_SUPPLIES_ARRAY_SIZE ] =
{
	"TC Modifier",
	"TC Occupied Drain",
	"Supply Distance Modifier",
	"Repair Modifier",
	"Energy To Oil",
	"Production Efficiency",
	"Supply Prod. Efficiency",
	"Manpower Growth",
	"Trickleback Modifier"
};
//=============================================================================================================
CVar_Base* CheckBoxMaxProductionAndSupplies[ PRODUCTION_AND_SUPPLIES_ARRAY_SIZE ] =
{
	new CVar ( "Max TC Modifier", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max TC Occupied Drain", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Supply Distance Modifier", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Repair Modifier", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Energy To Oil", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Production Efficiency", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Supply Prod. Efficiency", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Manpower Growth", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Trickleback Modifier", 0, 1, 0, TYPE_BOOL ),
};
//=============================================================================================================
CVar_Base* EditBoxProductionAndSupplies[ PRODUCTION_AND_SUPPLIES_ARRAY_SIZE ] =
{
	new CVar ( "Editbox TC Modifier", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox TC Occupied Drain", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Supply Distance Modifier", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Repair Modifier", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Energy To Oil", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Production Efficiency", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Supply Prod. Efficiency", 0, 10000, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Manpower Growth", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Trickleback Modifier", 0, 100, 0, TYPE_FLOAT ),
};
//=============================================================================================================
char* g_CombatEvents[ COMBAT_EVENTS_ARRAY_SIZE ] =
{
	"Counterattack",
	"Assault",
	"Encirclement",
	"Ambush",
	"Delay",
	"Tactical Withdrawal",
	"Breakthrough",
	"HQ Bonus Chance"
};
//=============================================================================================================
CVar_Base* CheckBoxMaxCombatEvents[ COMBAT_EVENTS_ARRAY_SIZE ] =
{
	new CVar ( "Max Counterattack", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Assault", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Encirclement", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Ambush", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Delay", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Tactical Withdrawal", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Breakthrough", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max HQ Bonus Chance", 0, 1, 0, TYPE_BOOL )
};
//=============================================================================================================
CVar_Base* EditBoxCombatEvents[ COMBAT_EVENTS_ARRAY_SIZE ] =
{
	new CVar ( "Editbox Counterattack", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Assault", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Encirclement", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Ambush", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Delay", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Tactical Withdrawal", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Breakthrough", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox HQ Bonus Chance", 0, 100, 0, TYPE_FLOAT )
};
//=============================================================================================================
char* g_MiscMilitary[ MISC_MILITARY_ARRAY_SIZE ] =
{
	"Armies can dig in",
	"Missiles may carry nukes",
	"Provincial AA Efficiency",
	"Attrition Modifier",
	"HQ ESE Bonus",
	"Ground Defense Efficiency",
	"Convoy Defense Efficiency",
	"Land Fort Efficiency",
	"Coast Fort Efficiency"
};
//=============================================================================================================
CVar_Base* CheckBoxMaxMiscMilitary[ MISC_MILITARY_ARRAY_SIZE ] =
{
	new CVar ( "Armies can dig in", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Missiles may carry nukes", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Provincial AA Efficiency", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Attrition Modifier", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max HQ ESE Bonus", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Ground Defense Efficiency", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Convoy Defense Efficiency", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Land Fort Efficiency", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Coast Fort Efficiency", 0, 1, 0, TYPE_BOOL )
};
//=============================================================================================================
CVar_Base* EditBoxMiscMilitary[ MISC_MILITARY_ARRAY_SIZE ] =
{
	new CVar ( "Editbox Armies can dig in", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Editbox Missiles may carry nukes", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Editbox Provincial AA Efficiency", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Attrition Modifier", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox HQ ESE Bonus", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Ground Defense Efficiency", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Convoy Defense Efficiency", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Land Fort Efficiency", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Coast Fort Efficiency", 0, 100, 0, TYPE_FLOAT ),
};
//=============================================================================================================
char* g_IntelAndResearch[ INTEL_AND_RESEARCH_ARRAY_SIZE ] =
{
	"Friendly Intel Chance",
	"Enemy Intel Chance",
	"Radar Efficiency",
	"Research Speed"
};
//=============================================================================================================
CVar_Base* CheckBoxMaxIntelAndResearch[ INTEL_AND_RESEARCH_ARRAY_SIZE ] =
{
	new CVar ( "Max Friendly Intel Chance", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Enemy Intel Chance", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Radar Efficiency", 0, 1, 0, TYPE_BOOL ),
	new CVar ( "Max Research Speed", 0, 1, 0, TYPE_BOOL )
};
//=============================================================================================================
CVar_Base* EditBoxIntelAndResearch[ INTEL_AND_RESEARCH_ARRAY_SIZE ] =
{
	new CVar ( "Editbox Friendly Intel Chance", 0, FLT_MAX, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Enemy Intel Chance", -FLT_MAX, 0, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Radar Efficiency", 0, 100, 0, TYPE_FLOAT ),
	new CVar ( "Editbox Research Speed", 0, FLT_MAX, 0, TYPE_FLOAT )
};
//=============================================================================================================
Tab_t g_Tabs[] =
{
	{ { "Resource Tab" }, 0, InitializeResourceTab, 1024, 768 },
	{ { "Technology Tab" }, 0, InitializeTechnologyTab, 1024, 768 },
	{ { "Province Tab" }, 0, InitializeProvinceTab, 1024, 768 },
	{ { "Unit Modifiers Tab" }, 0, InitializeUnitModifiersTab, 1024, 768 },
	{ { "Global Variables Tab" }, 0, InitializeGlobalVariablesTab, 1280, 1024 },
	{ { "Diplomacy Tab" }, 0, InitializeDiplomacyTab, 1024, 768 }
};
//=============================================================================================================