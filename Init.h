//=============================================================================================================
extern std::map< unsigned int, unsigned int > g_MapOffsets;
//=============================================================================================================
unsigned int HashString ( const char *String );
//=============================================================================================================
void InitializeOffsets ( void );
//=============================================================================================================
void InitializeResourceTab ( HINSTANCE hInstance );
void InitializeTechnologyTab ( HINSTANCE hInstance );
void InitializeProvinceTab ( HINSTANCE hInstance );
void InitializeUnitModifiersTab ( HINSTANCE hInstance );
void InitializeGlobalVariablesTab ( HINSTANCE hInstance );
void InitializeDiplomacyTab ( HINSTANCE hInstance );
//=============================================================================================================