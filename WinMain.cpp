//=============================================================================================================
#include "Required.h"
//=============================================================================================================
#include "Cvars.h"
#include "Defines.h"
#include "Globals.h"
#include "Hack.h"
//=============================================================================================================
HINSTANCE g_Instance;
const char g_ClassName[] = "Hearts Of Iron 2 Cheat";
//=============================================================================================================
void CreateTabs ( HINSTANCE hInstance )
{
	HWND Tab;
	RECT tr;

	tr = {0};

	TabCtrl_GetItemRect ( g_TabControl, 0, &tr );
	
	for ( size_t Iterator = 0; Iterator < END_TAB_ENUM; Iterator++ )
	{		
		Tab = CreateWindowExA(
			WS_EX_DLGMODALFRAME,
			"Static",
			"",
			WS_CHILD | WS_VISIBLE,
			0,
			tr.bottom - tr.top,
			WINDOW_SIZE_X,
			WINDOW_SIZE_Y - tr.bottom - tr.top,
			g_TabControl,
			0,
			hInstance,
			NULL
		);

		g_Tabs[ Iterator ].m_Window = Tab;

		g_Tabs[ Iterator ].m_Function ( hInstance );
	}
}
//=============================================================================================================
void CreateTabControl ( HINSTANCE hInstance, HWND MainWindow )
{
    TCITEM tie = {0};  // tab item structure

	tie.mask = TCIF_TEXT;

    g_TabControl = CreateWindowExA(
        WS_EX_CLIENTEDGE,		// extended style
        WC_TABCONTROL,			// tab control constant
        "",						// text/caption
        WS_CHILD | WS_VISIBLE,	// is a child control, and visible
        0,						// X position - device units from left
        0,						// Y position - device units from top
        WINDOW_SIZE_X,			// Width - in device units
        WINDOW_SIZE_Y,			// Height - in device units
        MainWindow,				// parent window
		0,						// no menu
        hInstance,				// instance
        NULL					// no extra junk
    );

	if ( g_TabControl == NULL )
	{
		return;
	}

	for ( size_t Iterator = 0; Iterator < END_TAB_ENUM; Iterator++ )
	{
		tie.pszText = g_Tabs[ Iterator ].m_TabName;

		if ( TabCtrl_InsertItem ( g_TabControl, Iterator, &tie ) == -1 )
		{
			DestroyWindow ( g_TabControl );

			return;
		}
	}

	RECT tr;

	tr = {0};

	TabCtrl_GetItemRect ( g_TabControl, 0, &tr );

	g_SubTabControl = CreateWindowExA(
        WS_EX_CLIENTEDGE,		// extended style
        WC_TABCONTROL,			// tab control constant
        "",						// text/caption
        WS_CHILD | WS_VISIBLE,	// is a child control, and visible
        0,						// X position - device units from left
        tr.bottom,					// Y position - device units from top
        WINDOW_SIZE_X,			// Width - in device units
        WINDOW_SIZE_Y,			// Height - in device units
        g_TabControl,				// parent window
		0,						// no menu
        hInstance,				// instance
        NULL					// no extra junk
    );

	for ( size_t Iterator = 0; Iterator < END_TAB_ENUM; Iterator++ )
	{
		tie.pszText = g_Tabs[ Iterator ].m_TabName;

		if ( TabCtrl_InsertItem ( g_SubTabControl, Iterator, &tie ) == -1 )
		{
			DestroyWindow ( g_TabControl );

			return;
		}
	}
}
//=============================================================================================================
LRESULT CALLBACK WndProc ( HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam )
{
	bool ResetPosition = false;
	
	RECT Window;

	LONG x, y;

	switch ( Message )
	{
		case WM_CREATE:
		{
			g_MainWindow = hwnd;

			CreateTabControl ( g_Instance, hwnd );
		
			CreateTabs ( g_Instance );
			
			for ( size_t Iterator = 0; Iterator < END_TAB_ENUM; Iterator++ )
			{
				ShowWindow ( g_Tabs[ Iterator ].m_Window, SW_HIDE );
			}

			ShowWindow ( g_Tabs[ TAB_RESOURCES ].m_Window, SW_SHOW );

			GetWindowRect ( hwnd, &Window );

			g_WindowX = Window.left;
			g_WindowY = Window.top;

			SetWindowPos ( hwnd, HWND_NOTOPMOST, g_WindowX, g_WindowY, g_Tabs[ TAB_RESOURCES ].m_Width, g_Tabs[ TAB_RESOURCES ].m_Height, 0 );
		}
		break;
		
		case WM_CLOSE:
		{
			DestroyWindow ( hwnd );
		}
		break;

		case WM_DESTROY:
		{
			PostQuitMessage ( 0 );
		}
		break;
		
		case WM_MOVE:
		{
			GetWindowRect ( hwnd, &Window );

			x = Window.left;
			y = Window.top;

			if ( Window.right > g_Desktop.m_Right )
			{
				ResetPosition = true;

				x = g_Desktop.m_Right - g_Tabs[ g_TabItem ].m_Width;
			}
			if ( Window.bottom > g_Desktop.m_Bottom )
			{
				ResetPosition = true;

				y = g_Desktop.m_Bottom - g_Tabs[ g_TabItem ].m_Height;
			}
			if ( Window.left < g_Desktop.m_Left )
			{
				ResetPosition = true;

				x = g_Desktop.m_Left;
			}
			if ( Window.top < g_Desktop.m_Top )
			{
				ResetPosition = true;

				y = g_Desktop.m_Top;
			}

			if ( ResetPosition )
			{
//				SetWindowPos ( hwnd, HWND_TOPMOST, x, y, WINDOW_SIZE_X, WINDOW_SIZE_Y, 0 );

				MoveWindow ( hwnd, x, y, g_Tabs[ g_TabItem ].m_Width, g_Tabs[ g_TabItem ].m_Height, true );
			}

			g_WindowX = x;
			g_WindowY = y;
		}
		break;
		
		case WM_NOTIFY:
		{
			LPNMHDR lpnmhdr = ( LPNMHDR )lParam;

			if ( lpnmhdr->code == TCN_SELCHANGE )
            {
				INT nTabItem = SendMessage ( g_TabControl, TCM_GETCURSEL, 0, 0 );

				for ( size_t Iterator = 0; Iterator < END_TAB_ENUM; Iterator++ )
				{
					if ( Iterator != nTabItem )
					{
						ShowWindow ( g_Tabs[ Iterator ].m_Window, SW_HIDE );
					}
					else
					{
						ShowWindow ( g_Tabs[ Iterator ].m_Window, SW_SHOW );

						SetWindowPos ( hwnd, HWND_NOTOPMOST, g_WindowX, g_WindowY, g_Tabs[ Iterator ].m_Width, g_Tabs[ Iterator ].m_Height, 0 );

						g_TabItem = Iterator;
					}
				}
			}
		}
		break;

		case WM_COMMAND:
		{
			switch ( LOWORD ( wParam ) )
			{

				default:
				{

				}
				break;
			}
			
		}
		break;

		default:
		{
			return DefWindowProcA ( hwnd, Message, wParam, lParam );
		}
	}
	return 0;
}
//=============================================================================================================
int WINAPI WinMain ( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{	
	WNDCLASSEX wc;
	HWND hwnd;
	MSG Msg;
	
	g_Instance = hInstance;

	RECT desktop;

	GetWindowRect ( GetDesktopWindow(), &desktop );

	g_Desktop.m_Left = desktop.left;
	g_Desktop.m_Top = desktop.top;
	g_Desktop.m_Right = desktop.right;
	g_Desktop.m_Bottom = desktop.bottom;

	wc.cbSize		 = sizeof ( WNDCLASSEX );
	wc.style		 = 0;
	wc.lpfnWndProc	 = WndProc;
	wc.cbClsExtra	 = 0;
	wc.cbWndExtra	 = 0;
	wc.hInstance	 = hInstance;
	wc.hIcon		 = 0;//LoadIcon ( g_hLocalInstance, MAKEINTRESOURCE ( IDI_MYICON ) );
	wc.hCursor		 = LoadCursor ( NULL, IDC_ARROW );
	wc.hbrBackground = ( HBRUSH )( COLOR_WINDOW + 1 );
	wc.lpszMenuName  = 0;//MAKEINTRESOURCE ( IDR_MYMENU );
	wc.lpszClassName = g_ClassName;
	wc.hIconSm		 = 0;//( HICON )LoadImage ( g_Instance, MAKEINTRESOURCE ( IDI_MYICON ), IMAGE_ICON, 16, 16, 0 );

	if ( !RegisterClassEx ( &wc ) )
	{
		return 0;
	}
	
	hwnd = CreateWindowExA ( WS_EX_CLIENTEDGE, g_ClassName, g_ClassName, WS_DLGFRAME | WS_OVERLAPPED | WS_CAPTION
		| WS_CLIPCHILDREN | WS_SYSMENU | WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, WINDOW_SIZE_X, WINDOW_SIZE_Y, 0, NULL, hInstance, NULL );

	if ( hwnd == NULL )
	{
		return 0;
	}
	
	BOOL ReturnedMessage;
	
	ShowWindow ( hwnd, SW_SHOW );

	g_ThreadHandle = CreateThread ( NULL, 0, ( LPTHREAD_START_ROUTINE )HackThread, NULL, 0, &g_ThreadID );
		
	do
	{
		ReturnedMessage = GetMessageA ( &Msg, hwnd, 0, 0 );

		TranslateMessage ( &Msg );
		DispatchMessage ( &Msg );

	}while ( ReturnedMessage != -1 && ReturnedMessage != 0 );

	return Msg.wParam;
}
//=============================================================================================================