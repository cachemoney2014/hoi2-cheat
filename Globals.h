//=============================================================================================================
#define AOD_NAME "aodgame.exe"
#define DH_NAME "Darkest Hour.exe"
#define HOI_NAME "HoI2.exe"
//=============================================================================================================
#define NOT_ACTIVE 0
#define ACTIVE_IN_EDITBOX ( 1 << 0 )
#define ACTIVE_FINISHED ( 1 << 1 )
//=============================================================================================================
#define RESOURCE_ARRAY_SIZE 10
#define MISSION_ARRAY_SIZE 34
#define PRODUCTION_AND_SUPPLIES_ARRAY_SIZE 9
#define COMBAT_EVENTS_ARRAY_SIZE 8
#define MISC_MILITARY_ARRAY_SIZE 9
#define INTEL_AND_RESEARCH_ARRAY_SIZE 4
//=============================================================================================================
extern int g_WindowX, g_WindowY;
extern int g_WindowSizeWidth, g_WindowHeight;
extern int g_TabItem;
//=============================================================================================================
#define ROUND_UP( p, align ) ( ((DWORD)(p) + (align)-1) & ~((align)-1) )
//=============================================================================================================
typedef struct
{
	long m_Left;
	long m_Top;
	long m_Right;
	long m_Bottom;
}Desktop_t;
//=============================================================================================================
typedef struct
{	
	int m_Active;
	CVar_Base* m_CVar;
	HWND m_Window;
	int m_HMENU;
	int m_Type;
	int m_SubType;
}ChildWindow_t;
//=============================================================================================================
typedef struct
{
	char* m_TabName;
	HWND m_Window;
	void ( *m_Function )( HINSTANCE );
	int m_Width;
	int m_Height;
	std::vector<ChildWindow_t> m_ChildWindows;
//	std::vector<CVar_Base*> m_CVars;
}Tab_t;
//=============================================================================================================
typedef struct
{
	float m_Oil;
	float m_Metal;
	float m_Energy;
	float m_Rare;
	float m_Supplies;
	float m_Money;
	float m_Manpower;

	int m_Nuke;
}Resources_t;
//=============================================================================================================
typedef struct
{
	float m_Dissent;
}GlobalVariables_t;
//=============================================================================================================
typedef struct
{
	float m_Attack;
	float m_Rebase;
	float m_StrategicRedeployment;
	float m_SupportAttack;
	float m_SupportDefense;
	float m_Reserves;
	float m_AntiPartisanDuty;
	float m_ArtilleryBombardment;
	float m_AirSuperiority;
	float m_GroundAttack;
	float m_Interdiction;
	float m_StrategicBomardment;
	float m_IndustrialBombardment;
	float m_LogisticalStrike;
	float m_AirportStrike;
	float m_installationStrike;
	float m_NavalStrike;
	float m_PortStrike;
	float m_BombConvoys;
	float m_AirSupply;
	float m_AirborneAssault;
	float m_Nuke;
	float m_ConvoyRaiding;
	float m_AntiSubarmineWarfare;
	float m_NavalInterdiction;
	float m_ShortBombardment;
	float m_AmphibiousAssault;
	float m_SeaTransport;
	float m_NavalCombatPatrol;
	float m_CarrierStrikeOnPort;
	float m_CarrierStrikeOnAirbase;

//	Darkest hour only

	float m_Sneakmove;
	float m_NavalScramble;
	short m_MaxAmphibiousSize;

}Missions_t;
//=============================================================================================================
typedef struct
{
	float m_TCModifier;
	float m_TCoccupiedDrain;
	float m_RepairModifier;
	float m_EnergyToOil;
	float m_ProductionEfficiency;
	float m_GearingLimit;
	float m_GearingEfficiency;
	float m_SupplyProductionEfficiency;
	float m_ManpowerGrowth;

//	Darkest hour only

	float m_TricklebackModifier;

}Production_And_Supplies_t;
//=============================================================================================================
typedef struct
{
	float m_FriendlyIntelChance;
	float m_EnemyIntelChance;
	float m_RadarEfficiency;
	float m_ResearchSpeed;
}Intel_And_Research;
//=============================================================================================================
typedef struct
{
	// todo:
}Naval_Positioning_t;
//=============================================================================================================
typedef struct
{
	bool m_ArmiesCanDigIn;
	bool m_MissilesMayCarryNukes;
	float m_ProvincialAAEffiency;
	float m_AttritionModifer;
	float m_EQESEBonus;
	float m_GroundDefenseEfficency;
	float m_LandFortEfficency;
	float m_CoastFortEfficency;
}Misc_Military_t;
//=============================================================================================================
typedef struct
{
	float m_CounterAttack;
	float m_Assault;
	float m_Encirclement;
	float m_Ambush;
	float m_Delay;
	float m_TacticalWithdrawal;
	float m_Breakthrough;
	float m_HQBonusChance;
}Combat_Events_t;
//=============================================================================================================
extern uintptr_t g_PlayerBase;
//=============================================================================================================
extern Resources_t g_Resources;
//=============================================================================================================
extern int g_GameID;
//=============================================================================================================
extern DWORD g_ThreadID;
//=============================================================================================================
extern Desktop_t g_Desktop;
//=============================================================================================================
extern Resources_t g_Resources;
//=============================================================================================================
extern char* g_ResourceNames[ RESOURCE_ARRAY_SIZE ];
extern char* g_MissionNames[ MISSION_ARRAY_SIZE ];
extern char* g_ProductionAndSupplies[ PRODUCTION_AND_SUPPLIES_ARRAY_SIZE ];
extern char* g_CombatEvents[ COMBAT_EVENTS_ARRAY_SIZE ];
extern char* g_MiscMilitary[ MISC_MILITARY_ARRAY_SIZE ];
extern char* g_IntelAndResearch[ INTEL_AND_RESEARCH_ARRAY_SIZE ];
//=============================================================================================================
extern HANDLE g_ThreadHandle;
extern HWND g_TabControl;
extern HWND g_SubTabControl;
extern HWND g_MainWindow;
extern Tab_t g_Tabs[];
//=============================================================================================================
extern CVar_Base* CheckBoxEnableMissions[ MISSION_ARRAY_SIZE ];
extern CVar_Base* CheckBoxFreezeResources[ RESOURCE_ARRAY_SIZE ];
extern CVar_Base* CheckBoxMaxCombatEvents[ COMBAT_EVENTS_ARRAY_SIZE ];
extern CVar_Base* CheckBoxMaxResources[ RESOURCE_ARRAY_SIZE ];
extern CVar_Base* CheckBoxMaxMissions[ MISSION_ARRAY_SIZE ];
extern CVar_Base* CheckBoxMaxProductionAndSupplies[ PRODUCTION_AND_SUPPLIES_ARRAY_SIZE ];
extern CVar_Base* CheckBoxMaxMiscMilitary[ MISC_MILITARY_ARRAY_SIZE ];
extern CVar_Base* CheckBoxMaxIntelAndResearch[ INTEL_AND_RESEARCH_ARRAY_SIZE ];
extern CVar_Base* EditBoxCombatEvents[ COMBAT_EVENTS_ARRAY_SIZE ];
extern CVar_Base* EditBoxResources[ RESOURCE_ARRAY_SIZE ];
extern CVar_Base* EditBoxMissions[ MISSION_ARRAY_SIZE ];
extern CVar_Base* EditBoxProductionAndSupplies[ PRODUCTION_AND_SUPPLIES_ARRAY_SIZE ];
extern CVar_Base* EditBoxMiscMilitary[ MISC_MILITARY_ARRAY_SIZE ];
extern CVar_Base* EditBoxIntelAndResearch[ INTEL_AND_RESEARCH_ARRAY_SIZE ];
//=============================================================================================================