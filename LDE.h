//=============================================================================================================
// group 1
//=============================================================================================================
#define PREFIX_SEGMENT_CS				0x2E
#define PREFIX_SEGMENT_SS				0x36
#define PREFIX_SEGMENT_DS				0x3E
#define PREFIX_SEGMENT_ES				0x26
#define PREFIX_SEGMENT_FS				0x64
#define PREFIX_SEGMENT_GS				0x65
#define PREFIX_BRANCH_NOT_TAKEN			0x2E
#define PREFIX_BRANCH_TAKEN				0x3E
#define PREFIX_WAIT						0x9B
//=============================================================================================================
// group 2
//=============================================================================================================
#define PREFIX_LOCK						0xF0
#define PREFIX_REPNZ					0xF2
#define PREFIX_REPX						0xF3
//=============================================================================================================
// group 3
//=============================================================================================================
#define PREFIX_OPERAND_SIZE				0x66
#define PREFIX_SSE_PRECISION_OVERRIDE	0x66
//=============================================================================================================
// group 4
//=============================================================================================================
#define PREFIX_ADDRESS_SIZE				0x67
//=============================================================================================================
#define PREFIX_REX						0x40
//=============================================================================================================
#define OPCODE_TWO_BYTE					0x0F
#define PREFIX_3DNOW					0x0F
//=============================================================================================================
#define FLAG_PREFIX_OVERRIDE_SIZE		( 1 << 0 )
#define	FLAG_PREFIX_OVERRIDE_ADDRESS	( 1 << 1 )
#define	FLAG_PREFIX_LOCK				( 1 << 2 )
#define	FLAG_PREFIX_REPEAT				( 1 << 3 )
#define	FLAG_PREFIX_SEGMENT				( 1 << 4 )
#define FLAG_PREFIX_WAIT				( 1 << 5 )
#define FLAG_PREFIX_REX					( 1 << 6 )
#define FLAG_PREFIX_0F					( 1 << 7 )
#define	FLAG_OPCODE2					( 1 << 8 )
#define	FLAG_MODRM						( 1 << 9 )
#define	FLAG_SIB						( 1 << 10 )
#define	FLAG_DISP8						( 1 << 11 )
#define	FLAG_DISP16						( 1 << 12 )
#define	FLAG_DISP32						( 1 << 13 )
#define FLAG_DISP64						( 1 << 14 )
#define	FLAG_IMM8						( 1 << 15 )
#define	FLAG_IMM16						( 1 << 16 )
#define	FLAG_IMM32						( 1 << 17 )
#define	FLAG_IMM64						( 1 << 18 )
#define	FLAG_REL8						( 1 << 19 )
#define	FLAG_REL16						( 1 << 20 )
#define	FLAG_REL32						( 1 << 21 )
#define FLAG_3DNOW						( 1 << 22 )
#define FLAG_IMM8_2						( 1 << 23 )
#define FLAG_IMM16_2					( 1 << 24 )
#define FLAG_IMM32_2					( 1 << 25 )
#define FLAG_IMM64_2					( 1 << 26 )
#define FLAG_SEGMENT_REL				( 1 << 27 )
#define FLAG_OPCEXT						( 1 << 28 )
#define FLAG_SEGMENT_REGISTER			( 1 << 29 )
#define FLAG_FPU						( 1 << 30 )
//=============================================================================================================
#define MAX_INSTRUCTION_SIZE			16
//=============================================================================================================
typedef struct
{
	uintptr_t m_CurrentPointer;

	unsigned char m_Prefix;
	unsigned char m_Prefix0F;
	unsigned char m_Prefix26;
	unsigned char m_Prefix2E;
	unsigned char m_Prefix36;
	unsigned char m_Prefix3E;
	unsigned char m_Prefix64;
	unsigned char m_Prefix65;
	unsigned char m_Prefix66;
	unsigned char m_Prefix67;
	unsigned char m_Prefix9B;
	unsigned char m_PrefixF0;
	unsigned char m_PrefixF2;
	unsigned char m_PrefixF3;
	unsigned char m_PrefixREX;
	unsigned char m_REXPrefix;

	unsigned char m_PrefixLock;
	unsigned char m_PrefixRepeat;
	unsigned char m_PrefixSegment;
	unsigned char m_PrefixWait;
	unsigned char m_PrefixOperandSizeOverride;
	unsigned char m_PrefixAddressSizeOverride;
	unsigned char m_PrefixBranchTaken;
	unsigned char m_PrefixBranchNotTaken;
	unsigned char m_Prefix3DNow;

	unsigned char m_Opcode;
	unsigned char m_Opcode2;
	unsigned char m_ModRM;
	unsigned char m_SIB;

	unsigned char m_Register;
	unsigned char m_IsRegisterInOpcode;

	unsigned char m_ModRM_Mod;
	unsigned char m_ModRM_Reg;
	unsigned char m_ModRM_RM;

	unsigned char m_RMRegister;

	unsigned char m_SIB_Scale;
	unsigned char m_SIB_Index;
	unsigned char m_SIB_Base;

	unsigned char m_BaseRegister;

	unsigned char m_Scalar;

	unsigned char m_HasDisplacement;
	unsigned char m_HasImmediate;
	unsigned char m_HasRelative;

	unsigned char m_UseRegisterToRegister;
	unsigned char m_UsingSIB;

	unsigned char m_UseDisplacementOnly;
	unsigned char m_DisplacementSize;

	unsigned short m_Segment;
	
	unsigned char m_IsPrefixAddressSizeOverrideValid;
	unsigned char m_IsPrefixOperandSizeOverideValid;
	unsigned char m_IsPrefixRepeatValid;
	unsigned char m_IsLockPrefixValid;
	unsigned char m_HasDirectionFlag;

	union
	{
		unsigned char Displacement8;
		unsigned short Displacement16;
		unsigned long Displacement32;
		unsigned long Displacement64;
	}Displacement;

	union
	{
		unsigned char Immediate8;
		unsigned short Immediate16;
		unsigned long Immediate32;
		unsigned long long Immediate64;
	}Immediate;

	union
	{
		unsigned char Immediate8;
		unsigned short Immediate16;
		unsigned long Immediate32;
		unsigned long long Immediate64;
	}Immediate2;

	union
	{
		signed char Relative8;
		signed short Relative16;
		signed long Relative32;
	}Relative;

	
	unsigned short m_REX_W; // Operand size. 0: Default, 1: 64 bit
	unsigned short m_REX_R; // ModRM.reg extension
	unsigned short m_REX_X; // SIB.index extension
	unsigned short m_REX_B; // ModRM.rm extension

	int m_Flags;
	int m_Length;
	
}LDEDisasm_t;
//=============================================================================================================
#define MODE_32	1
#define MODE_64	2
//=============================================================================================================
#define REG_ESP	4
#define REG_EBP	5
#define ALL_GENERAL_PURPOSE 127
#define REG_INVALID	255
//=============================================================================================================
int LDEDisasm ( _Inout_ LDEDisasm_t* pDisasm, _In_ unsigned char* Address, _In_ int Mode );
//=============================================================================================================
void LDEDisasm32 ( _Inout_ LDEDisasm_t* pDisasm, _Inout_ unsigned char*& CurrentPointer, _In_ unsigned char CurrentValue );
void LDEDisasm64 ( _Inout_ LDEDisasm_t* pDisasm, _Inout_ unsigned char*& CurrentPointer, _In_ unsigned char CurrentValue );
void LDEModRMAndSIB3264 ( _Inout_ LDEDisasm_t* pDisasm, _Inout_ unsigned char*& CurrentPointer );
void LDEDecodeSIB3264 ( _Inout_ LDEDisasm_t* pDisasm );
void LDEDecodeModRM3264 ( _Inout_ LDEDisasm_t* pDisasm );
//=============================================================================================================