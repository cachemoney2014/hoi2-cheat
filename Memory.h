//=============================================================================================================
class CMemory
{

public:

	CMemory();
	~CMemory();
	
	BOOL Read ( LPCVOID Address, SIZE_T Size, LPVOID Out );
	BOOL Write ( LPVOID Address, SIZE_T Size, LPCVOID Out );

	void Refresh();

	void Set ( HANDLE Process );

private:

	char* m_Executable, *m_Module;

	HANDLE m_Process;

	SIZE_T BytesRead;
	SIZE_T BytesWritten;

	void* m_pBuffer;
};
//=============================================================================================================
BYTE GetNeutralizedModRM ( LDEDisasm_t* pDisasm );
BYTE GetNeutralizedOpcode32 ( LDEDisasm_t* pDisasm );
BYTE GetNeutralizedSIB ( LDEDisasm_t* pDisasm );
//=============================================================================================================	
size_t GetAdjustedSignatureSize ( PBYTE pbSignatureIn, size_t uiSizeOfSignature );
//=============================================================================================================
void Assemble32 ( PBYTE Output, LDEDisasm_t* pDisasm, size_t NeutralizeFlags );
void AssemblePatternMask32 ( PBYTE Output, LDEDisasm_t* pDisasm, size_t NeutralizeFlags );
void CreatePattern32 ( PBYTE SignatureIn, size_t SizeOfSignature, size_t NeutralizeFlags );
void CreatePatternMask32 ( PBYTE SignatureIn, size_t SizeOfSignature, size_t NeutralizeFlags );
//=============================================================================================================
extern CMemory g_Memory;
//=============================================================================================================