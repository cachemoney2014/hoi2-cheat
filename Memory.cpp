//=============================================================================================================
#include "Required.h"
//=============================================================================================================
#include "Defines.h"
#include "LDE.h"
#include "Memory.h"
//=============================================================================================================
CMemory g_Memory;
//=============================================================================================================
CMemory::CMemory()
{

};
//=============================================================================================================
CMemory::~CMemory()
{

};
//=============================================================================================================
BOOL CMemory::Read ( LPCVOID Address, SIZE_T Size, LPVOID Out )
{
	return ReadProcessMemory ( m_Process, Address, Out, Size, &BytesRead );
}
//=============================================================================================================
BOOL CMemory::Write ( LPVOID Address, SIZE_T Size, LPCVOID Out )
{
	return WriteProcessMemory ( m_Process, Address, Out, Size, &BytesWritten );
}
//=============================================================================================================
void CMemory::Set ( HANDLE Process )
{
	m_Process = Process;
}
//=============================================================================================================
void Assemble32 ( PBYTE Output, LDEDisasm_t* pDisasm, size_t NeutralizeFlags )
{
	if ( pDisasm->m_Flags & FLAG_PREFIX_OVERRIDE_SIZE )
	{
		*Output++ = pDisasm->m_PrefixOperandSizeOverride;
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_OVERRIDE_ADDRESS ) 
	{	
		*Output++ = pDisasm->m_PrefixAddressSizeOverride;
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_LOCK )
	{
		*Output++ = pDisasm->m_PrefixLock;
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_REPEAT )
	{
		*Output++ = pDisasm->m_PrefixRepeat;
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_SEGMENT )
	{
		*Output++ = pDisasm->m_PrefixSegment;
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_WAIT )
	{
		*Output++ = pDisasm->m_PrefixWait;
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_0F )
	{
		*Output++ = pDisasm->m_Prefix0F;
	}

	if ( pDisasm->m_Flags & FLAG_3DNOW )
	{
		*Output++ = pDisasm->m_Prefix3DNow;

		if ( pDisasm->m_Flags & FLAG_MODRM )
		{			
			*Output++ = GetNeutralizedModRM ( pDisasm );
		}
		if ( pDisasm->m_Flags & FLAG_SIB )
		{
			*Output++ = GetNeutralizedSIB ( pDisasm );
		}

		*Output++ = GetNeutralizedOpcode32 ( pDisasm );
	}
	else
	{
			
		*Output++ = GetNeutralizedOpcode32 ( pDisasm );
	
		if ( pDisasm->m_Flags & FLAG_OPCODE2 )
		{		
			*Output++ = pDisasm->m_Opcode2;
		}
		if ( pDisasm->m_Flags & FLAG_MODRM )
		{
			*Output++ = GetNeutralizedModRM ( pDisasm );
		}
		if ( pDisasm->m_Flags & FLAG_SIB )
		{
			*Output++ = GetNeutralizedSIB ( pDisasm );
		}
		if ( pDisasm->m_Flags & FLAG_DISP8 )
		{
			*Output++ = 0xCC;
			}
		else if ( pDisasm->m_Flags & FLAG_DISP16 )
		{
			memset ( Output, 0xCC, sizeof ( uint16_t ) );
					
			Output += sizeof ( uint16_t );
		}
		else if ( pDisasm->m_Flags & FLAG_DISP32 )
		{
			memset ( Output, 0xCC, sizeof ( uint32_t ) );
				
			Output += sizeof ( uint32_t );
		}
		if ( pDisasm->m_Flags & FLAG_IMM8 )
		{
			*Output++ = 0xCC;
		}
		else if ( pDisasm->m_Flags & FLAG_IMM16 )
		{
			memset ( Output, 0xCC, sizeof ( uint32_t ) );
					
			Output += sizeof ( uint16_t );
		}
		else if ( pDisasm->m_Flags & FLAG_IMM32 )
		{
			memset ( Output, 0xCC, sizeof ( uint32_t ) );
			
			Output += sizeof ( uint32_t );
		}
		if ( pDisasm->m_Flags & FLAG_IMM8_2 )
		{
			*Output++ = 0xCC;
		}
		else if ( pDisasm->m_Flags & FLAG_IMM16_2 )
		{
			memset ( Output, 0xCC, sizeof ( uint16_t ) );
				
			Output += sizeof ( uint16_t );
		}
		else if ( pDisasm->m_Flags & FLAG_IMM32_2 )
		{
			memset ( Output, 0xCC, sizeof ( uint32_t ) );
		
			Output += sizeof ( uint32_t );
		}
		if ( pDisasm->m_Flags & FLAG_REL8 )
		{
			*Output++ = 0xCC;
		}
		else if ( pDisasm->m_Flags & FLAG_REL32 )
		{
			memset ( Output, 0xCC, sizeof ( uint32_t ) );
				
			Output += sizeof ( uint32_t );
		}
		else if ( pDisasm->m_Flags & FLAG_SEGMENT_REL )
		{
			memset ( Output, 0xCC, sizeof ( uint16_t ) );
		
			Output += sizeof ( uint16_t );
		}
	}

	return;
}
//=============================================================================================================
void AssemblePatternMask32 ( PBYTE Output, LDEDisasm_t* pDisasm, size_t NeutralizeFlags )
{
	if ( pDisasm->m_Flags & FLAG_PREFIX_SEGMENT )
	{
		*Output++ = 0x3F;
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_LOCK )
	{
		*Output++ = 0x3F;
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_REPEAT )
	{
		*Output++ = 0x3F;
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_OVERRIDE_SIZE )
	{
		*Output++ = 0x3F;
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_OVERRIDE_ADDRESS  ) 
	{	
		*Output++ = 0x3F;
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_WAIT ) 
	{	
		*Output++ = 0x3F;
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_0F )
	{
		*Output++ = 0x78;
	}
	
	*Output++ = 0x78;
	
	if ( pDisasm->m_Flags & FLAG_OPCODE2 )
	{
		*Output++ = 0x3F;
	}
	if ( pDisasm->m_Flags & FLAG_MODRM )
	{
		*Output++ = 0x3F;

	}
	if ( pDisasm->m_Flags & FLAG_SIB )
	{
		*Output++ = 0x3F;
	}
	if ( pDisasm->m_Flags & FLAG_DISP8 )
	{
		*Output++ = 0x3F;
	}
	else if ( pDisasm->m_Flags & FLAG_DISP16 )
	{
		memset ( Output, 0x3F, sizeof ( uint32_t ) );
								
		Output += sizeof ( uint16_t );
	}
	else if ( pDisasm->m_Flags & FLAG_DISP32 )
	{
		memset ( Output, 0x3F, sizeof ( uint32_t ) );
						
		Output += sizeof ( uint32_t );
	}
	if ( pDisasm->m_Flags & FLAG_IMM8 )
	{
		*Output++ = 0x3F;
	}
	else if ( pDisasm->m_Flags & FLAG_IMM16 )
	{
		memset ( Output, 0x3F, sizeof ( uint32_t ) );
						
		Output += sizeof ( uint16_t );
	}
	else if ( pDisasm->m_Flags & FLAG_IMM32 )
	{			
		memset ( Output, 0x3F, sizeof ( uint32_t ) );
			
		Output += sizeof ( uint32_t );
	}
	if ( pDisasm->m_Flags & FLAG_REL8 )
	{
		*Output++ = 0x3F;
	}
	else if ( pDisasm->m_Flags & FLAG_REL32 )
	{		
		memset ( Output, 0x3F, sizeof ( uint32_t ) );
						
		Output += sizeof ( uint32_t );
	}

	return;
}
//=============================================================================================================
void CreatePattern32 ( PBYTE SignatureIn, size_t SizeOfSignature, size_t NeutralizeFlags )
{
	PBYTE CurrentPointer;

	size_t Position;

	int CurrentLength;
	
	CurrentPointer = SignatureIn;

	Position = 0;

	LDEDisasm_t Disasm;

	memset ( &Disasm, 0, sizeof ( LDEDisasm_t ) );

	do
	{		
		memset ( &Disasm, 0, sizeof ( LDEDisasm_t ) );

		CurrentLength = LDEDisasm ( &Disasm, ( unsigned char* )CurrentPointer, MODE_32 );

		if ( CurrentLength != -1 )
		{													
			Assemble32 ( CurrentPointer, &Disasm, NeutralizeFlags );
																									
			CurrentPointer += CurrentLength;

			Position += CurrentLength;
		}
		else
		{
			Position++;

			CurrentPointer++;
		}
				
	}while ( Position < SizeOfSignature );
}
//=============================================================================================================
void CreatePatternMask32 ( PBYTE SignatureIn, size_t SizeOfSignature, size_t NeutralizeFlags )
{
	PBYTE CurrentPointer;

	size_t Position;

	int CurrentLength;
	
	CurrentPointer = SignatureIn;

	Position = 0;

	LDEDisasm_t Disasm;

	memset ( &Disasm, 0, sizeof ( LDEDisasm_t ) );

	do
	{		
		memset ( &Disasm, 0, sizeof ( LDEDisasm_t ) );

		CurrentLength = LDEDisasm ( &Disasm, ( unsigned char* )CurrentPointer, MODE_32 );

		if ( CurrentLength != -1 )
		{
			Position += CurrentLength;
						
			AssemblePatternMask32 ( CurrentPointer, &Disasm, NeutralizeFlags );
																									
			CurrentPointer += CurrentLength;
		}
		else
		{
			Position++;

			CurrentPointer++;
		}
				
	}while ( Position < SizeOfSignature );
}
//=============================================================================================================
size_t GetAdjustedSignatureSize ( PBYTE pbSignatureIn, size_t uiSizeOfSignature )
{
	PBYTE CurrentPointer;

	size_t Position;

	int CurrentLength;
	
	CurrentPointer = pbSignatureIn;

	Position = 0;

	LDEDisasm_t Disasm;

	memset ( &Disasm, 0, sizeof ( LDEDisasm_t ) );

	do
	{		
		memset ( &Disasm, 0, sizeof ( LDEDisasm_t ) );

		CurrentLength = LDEDisasm ( &Disasm, ( unsigned char* )CurrentPointer, MODE_32 );

		if ( CurrentLength != -1 )
		{
			Position += CurrentLength;

			CurrentPointer += CurrentLength;
		}
		else
		{
			Position++;

			CurrentPointer++;
		}
				
	}while ( Position < uiSizeOfSignature );

	return Position;
}
//=============================================================================================================
BYTE GetNeutralizedModRM ( LDEDisasm_t* pDisasm )
{
	BYTE NeutralizedModRM = 0;

	NeutralizedModRM = pDisasm->m_ModRM & 0xC0;

	if ( pDisasm->m_Flags & FLAG_MODRM )
	{				
		if ( pDisasm->m_Flags & FLAG_OPCEXT ) 
		{
/*
			if ( pDisasm->m_OpcodeFlags & OPC_X87 )
			{
				if ( pDisasm->m_OpcodeDefinition.m_Flags & OPC_MODRM_RANGE )
				{
					NeutralizedModRM |= pDisasm->m_ModRM_Reg << 3;

					if ( pDisasm->m_Flags & FLAG_SIB || pDisasm->m_ModRM_Mod == 0 && pDisasm->m_ModRM_RM == 5 )
					{
						NeutralizedModRM |= pDisasm->m_ModRM_RM;
					}
				}
				else if ( pDisasm->m_OpcodeDefinition.m_Flags & OPC_MODRM_SPECIFIED )
				{
					NeutralizedModRM |= pDisasm->m_ModRM_Reg << 3;
					NeutralizedModRM |= pDisasm->m_ModRM_RM;
				}
			}
			else
			{
*/
				if ( pDisasm->m_Flags & FLAG_SIB || pDisasm->m_ModRM_Mod == 0 && pDisasm->m_ModRM_RM == 5 )
				{
					NeutralizedModRM |= pDisasm->m_ModRM_RM;
				}
//			}
		}
		else
		{
			if ( pDisasm->m_Flags & FLAG_SIB || pDisasm->m_ModRM_Mod == 0 && pDisasm->m_ModRM_RM == 5 )
			{
				NeutralizedModRM |= pDisasm->m_ModRM_RM;
			}
		}
	}

	if ( pDisasm->m_Flags & FLAG_SIB || pDisasm->m_ModRM_Mod == 0 && pDisasm->m_ModRM_RM == 5 )
	{
		NeutralizedModRM |= pDisasm->m_ModRM_RM;
	}
		
	return NeutralizedModRM;
}
//=============================================================================================================
BYTE GetNeutralizedOpcode32 ( LDEDisasm_t* pDisasm )
{
	BYTE Opcode = pDisasm->m_Opcode;

	if ( pDisasm->m_IsRegisterInOpcode == true )
	{
		if ( ( Opcode & 0x0F ) < 9 )
		{
			Opcode &= 0xF0;
		}
		else
		{
			Opcode &= 0xF8;
		}
	}

	if ( pDisasm->m_HasDirectionFlag== 'D' )
	{
		Opcode &= 0xFD; // strip direction flag
	}

	return Opcode;
}
//=============================================================================================================
BYTE GetNeutralizedSIB ( LDEDisasm_t* pDisasm )
{
	BYTE NeutralizedSIB = 0;

	NeutralizedSIB = pDisasm->m_SIB & 0xC0;

	if ( pDisasm->m_SIB_Base == 5 )
	{
		NeutralizedSIB |= pDisasm->m_SIB_Base;
	}
		
	return NeutralizedSIB;
}
//=============================================================================================================