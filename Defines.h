//=============================================================================================================
#define WINDOW_SIZE_X 2560
#define WINDOW_SIZE_Y 1600
//=============================================================================================================
#define GAME_HOI2_BASE 1
#define GAME_HOI2_ARMAGEDDON 2
#define GAME_HOI2_AOD 3
#define GAME_HOI2_DARKESTHOUR 4
//=============================================================================================================
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned long uint32_t;
typedef unsigned long long uint64_t;
//=============================================================================================================
enum
{
	EDITBOX = 1,
	CHECKBOX = 2
};
//=============================================================================================================
enum
{
	CHECKBOX_ENABLED = ( 1 << 0 ),
	CHECKBOX_FREEZE = ( 1 << 1 ),
	CHECKBOX_MAX = ( 1 << 2 )
};
//=============================================================================================================
enum
{
	TAB_RESOURCES = 0,
	TAB_TECHNOLOGY,
	TAB_PROVINCE,
	TAB_UNITMODIFIERS,
	TAB_GLOBALVARIABLES,
	TAB_DIPLOMACY,
	END_TAB_ENUM,
};
//=============================================================================================================
enum
{
	EDITBOX_RESOURCE_OIL = END_TAB_ENUM,
	EDITBOX_RESOURCE_METAL,
	EDITBOX_RESOURCE_ENERGY,
	EDITBOX_RESOURCE_RARE,
	EDITBOX_RESOURCE_SUPPLIES,
	EDITBOX_RESOURCE_MONEY,
	EDITBOX_RESOURCE_MANPOWER,
	EDITBOX_RESOURCE_NUKE,
	EDITBOX_RESOURCE_BASE_IC,
	EDITBOX_RESOURCE_TOTAL_IC,
	END_EDITBOX_RESOURCE_ENUM
};
//=============================================================================================================
enum
{
	CHECKBOX_FREEZE_RESOURCE_OIL = END_EDITBOX_RESOURCE_ENUM,
	CHECKBOX_FREEZE_RESOURCE_METAL,
	CHECKBOX_FREEZE_RESOURCE_ENERGY,
	CHECKBOX_FREEZE_RESOURCE_RARE,
	CHECKBOX_FREEZE_RESOURCE_SUPPLIES,
	CHECKBOX_FREEZE_RESOURCE_MONEY,
	CHECKBOX_FREEZE_RESOURCE_MANPOWER,
	CHECKBOX_FREEZE_RESOURCE_NUKE,
	CHECKBOX_FREEZE_RESOURCE_BASE_IC,
	CHECKBOX_FREEZE_RESOURCE_TOTAL_IC,
	END_CHECKBOX_FREEZE_RESOURCE_ENUM
};
//=============================================================================================================
enum
{
	CHECKBOX_MAX_RESOURCE_OIL = END_CHECKBOX_FREEZE_RESOURCE_ENUM,
	CHECKBOX_MAX_RESOURCE_METAL,
	CHECKBOX_MAX_RESOURCE_ENERGY,
	CHECKBOX_MAX_RESOURCE_RARE,
	CHECKBOX_MAX_RESOURCE_SUPPLIES,
	CHECKBOX_MAX_RESOURCE_MONEY,
	CHECKBOX_MAX_RESOURCE_MANPOWER,
	CHECKBOX_MAX_RESOURCE_NUKE,
	CHECKBOX_MAX_RESOURCE_BASE_IC,
	CHECKBOX_MAX_RESOURCE_TOTAL_IC,
	END_CHECKBOX_MAX_RESOURCE_ENUM
};
//=============================================================================================================
enum
{
	EDITBOX_MISSIONS_MISSION_ATTACK = END_CHECKBOX_MAX_RESOURCE_ENUM,
	EDITBOX_MISSIONS_MISSION_REBASE,
	EDITBOX_MISSIONS_MISSION_STRATEGIC_REDEPLOYMENT,
	EDITBOX_MISSIONS_MISSION_SUPPORT_ATTACK,
	EDITBOX_MISSIONS_MISSION_SUPPORT_DEFENSE,
	EDITBOX_MISSIONS_RESERVES,
	EDITBOX_MISSIONS_ANTI_PARTISAN_DUTY,
	EDITBOX_MISSIONS_PLANNED_DEFENSE,
	EDITBOX_MISSIONS_AIR_SUPERIORITY,
	EDITBOX_MISSIONS_GROUND_ATTACK,
	EDITBOX_MISSIONS_GROUND_SUPPORT,
	EDITBOX_MISSIONS_STRATEGIC_BOMBARDMENT,
	EDITBOX_MISSIONS_LOGISTICAL_STRIKE,
	EDITBOX_MISSIONS_INSTALLATION_STRIKE,
	EDITBOX_MISSIONS_NAVAL_STRIKE,
	EDITBOX_MISSIONS_PORT_STRIKE,
	EDITBOX_MISSIONS_BOMB_CONVOYS,
	EDITBOX_MISSIONS_AIR_SUPPLY,
	EDITBOX_MISSIONS_AIRBORNE_ASSAULT,
	EDITBOX_MISSIONS_NUKE,
	EDITBOX_MISSIONS_AIR_SCRAMBLE,
	EDITBOX_MISSIONS_CONVOY_RAIDING,
	EDITBOX_MISSIONS_ASW,
	EDITBOX_MISSIONS_NAVAL_INTERDICTION,
	EDITBOX_MISSIONS_SHORE_BOMBARDMENT,
	EDITBOX_MISSIONS_AMPHIBIOUS_ASSAULT,
	EDITBOX_MISSIONS_SEA_TRANSPORT,
	EDITBOX_MISSIONS_NAVAL_COMBAT_PATROL,
	EDITBOX_MISSIONS_CARRIER_STRIKE_ON_PORT,
	EDITBOX_MISSIONS_CARRIER_STRIKE_ON_AIRBASE,
	EDITBOX_MISSIONS_SNEAK_MOVE,
	EDITBOX_MISSIONS_NAVAL_SCRAMBLE,
	EDITBOX_MISSIONS_MAX_AMPHIBIOUS_SIZE,

	END_EDITBOX_MISSIONS_ENUM,

	EDITBOX_PRODUCTION_AND_SUPPLIES_TC_MODIFIER = END_EDITBOX_MISSIONS_ENUM,
	EDITBOX_PRODUCTION_AND_SUPPLIES_TC_OCCUPIED_DRAIN,
	EDITBOX_PRODUCTION_AND_SUPPLIES_SUPPLY_DISTANCE_MODIFIER,
	EDITBOX_PRODUCTION_AND_SUPPLIES_REPAIR_MODIFIER,
	EDITBOX_PRODUCTION_AND_SUPPLIES_ENERGY_TO_OIL,
	EDITBOX_PRODUCTION_AND_SUPPLIES_PRODUCTION_EFFICIENCY,
	EDITBOX_PRODUCTION_AND_SUPPLIES_SUPPLY_PRODUCTION_EFFICIENCY,
	EDITBOX_PRODUCTION_AND_SUPPLIES_MANPOWER_GROWTH,
	EDITBOX_PRODUCTION_AND_SUPPLIES_TRICKLEBACK_MODIFIER,

	END_EDITBOX_PRODUCTION_AND_SUPPLIES_ENUM,

	EDITBOX_COMBAT_EVENTS_COUNTERATTACK = END_EDITBOX_PRODUCTION_AND_SUPPLIES_ENUM,
	EDITBOX_COMBAT_EVENTS_ASSAULT,
	EDITBOX_COMBAT_EVENTS_ENCIRCLEMENT,
	EDITBOX_COMBAT_EVENTS_AMBUSH,
	EDITBOX_COMBAT_EVENTS_DELAY,
	EDITBOX_COMBAT_EVENTS_TACTICAL_WITHDRAWAL,
	EDITBOX_COMBAT_EVENTS_BREAKTHROUGH,
	EDITBOX_COMBAT_EVENTS_HQ_BONUS_CHANCE,

	END_EDITBOX_COMBAT_EVENTS_ENUM,
	
	EDITBOX_MISC_MILITARY_ARMIES_CAN_DIG_IN = END_EDITBOX_COMBAT_EVENTS_ENUM,
	EDITBOX_MISC_MILITARY_MISSILES_CAN_CARRY_NUKES,
	EDITBOX_MISC_MILITARY_PROVINCIAL_AA_EFFICIENCY,
	EDITBOX_MISC_MILITARY_ATTRITION_MODIFIER,
	EDITBOX_MISC_MILITARY_HQ_ESE_BONUS,
	EDITBOX_MISC_MILITARY_GROUND_DEFENSE_EFFICIENCY,
	EDITBOX_MISC_MILITARY_CONVOY_DEFENSE_EFFICIENCY,
	EDITBOX_MISC_MILITARY_LAND_FORT_EFFICIENCY,
	EDITBOX_MISC_MILITARY_COAST_FORT_EFFICIENCY,

	END_EDITBOX_MISC_MILITARY_ENUM,
	
	EDITBOX_INTEL_AND_RESEARCH_FRIENDLY_INTEL_CHANCE = END_EDITBOX_MISC_MILITARY_ENUM,
	EDITBOX_INTEL_AND_RESEARCH_ENEMY_INTEL_CHANCE,
	EDITBOX_INTEL_AND_RESEARCH_RADAR_EFFICIENCY,
	EDITBOX_INTEL_AND_RESEARCH_RESEARCH_SPEED,

	END_EDITBOX_TECHNOLOGY_OVERVIEW_ENUM
};
//=============================================================================================================
enum
{
	CHECKBOX_MISSIONS_MISSION_ATTACK = END_EDITBOX_TECHNOLOGY_OVERVIEW_ENUM,
	CHECKBOX_MISSIONS_MISSION_REBASE,
	CHECKBOX_MISSIONS_MISSION_STRATEGIC_REDEPLOYMENT,
	CHECKBOX_MISSIONS_MISSION_SUPPORT_ATTACK,
	CHECKBOX_MISSIONS_MISSION_SUPPORT_DEFENSE,
	CHECKBOX_MISSIONS_RESERVES,
	CHECKBOX_MISSIONS_ANTI_PARTISAN_DUTY,
	CHECKBOX_MISSIONS_PLANNED_DEFENSE,
	CHECKBOX_MISSIONS_AIR_SUPERIORITY,
	CHECKBOX_MISSIONS_GROUND_ATTACK,
	CHECKBOX_MISSIONS_GROUND_SUPPORT,
	CHECKBOX_MISSIONS_STRATEGIC_BOMBARDMENT,
	CHECKBOX_MISSIONS_LOGISTICAL_STRIKE,
	CHECKBOX_MISSIONS_INSTALLATION_STRIKE,
	CHECKBOX_MISSIONS_NAVAL_STRIKE,
	CHECKBOX_MISSIONS_PORT_STRIKE,
	CHECKBOX_MISSIONS_BOMB_CONVOYS,
	CHECKBOX_MISSIONS_AIR_SUPPLY,
	CHECKBOX_MISSIONS_AIRBORNE_ASSAULT,
	CHECKBOX_MISSIONS_NUKE,
	CHECKBOX_MISSIONS_AIR_SCRAMBLE,
	CHECKBOX_MISSIONS_CONVOY_RAIDING,
	CHECKBOX_MISSIONS_ASW,
	CHECKBOX_MISSIONS_NAVAL_INTERDICTION,
	CHECKBOX_MISSIONS_SHORE_BOMBARDMENT,
	CHECKBOX_MISSIONS_AMPHIBIOUS_ASSAULT,
	CHECKBOX_MISSIONS_SEA_TRANSPORT,
	CHECKBOX_MISSIONS_NAVAL_COMBAT_PATROL,
	CHECKBOX_MISSIONS_CARRIER_STRIKE_ON_PORT,
	CHECKBOX_MISSIONS_CARRIER_STRIKE_ON_AIRBASE,
	CHECKBOX_MISSIONS_SNEAK_MOVE,
	CHECKBOX_MISSIONS_NAVAL_SCRAMBLE,
	CHECKBOX_MISSIONS_MAX_AMPHIBIOUS_SIZE,

	END_CHECKBOX_MISSIONS_ENUM,

	CHECKBOX_PRODUCTION_AND_SUPPLIES_TC_MODIFIER = END_CHECKBOX_MISSIONS_ENUM,
	CHECKBOX_PRODUCTION_AND_SUPPLIES_TC_OCCUPIED_DRAIN,
	CHECKBOX_PRODUCTION_AND_SUPPLIES_SUPPLY_DISTANCE_MODIFIER,
	CHECKBOX_PRODUCTION_AND_SUPPLIES_REPAIR_MODIFIER,
	CHECKBOX_PRODUCTION_AND_SUPPLIES_ENERGY_TO_OIL,
	CHECKBOX_PRODUCTION_AND_SUPPLIES_PRODUCTION_EFFICIENCY,
	CHECKBOX_PRODUCTION_AND_SUPPLIES_SUPPLY_PRODUCTION_EFFICIENCY,
	CHECKBOX_PRODUCTION_AND_SUPPLIES_MANPOWER_GROWTH,
	CHECKBOX_PRODUCTION_AND_SUPPLIES_TRICKLEBACK_MODIFIER,

	END_CHECKBOX_PRODUCTION_AND_SUPPLIES_ENUM,

	CHECKBOX_COMBAT_EVENTS_COUNTERATTACK = END_CHECKBOX_PRODUCTION_AND_SUPPLIES_ENUM,
	CHECKBOX_COMBAT_EVENTS_ASSAULT,
	CHECKBOX_COMBAT_EVENTS_ENCIRCLEMENT,
	CHECKBOX_COMBAT_EVENTS_AMBUSH,
	CHECKBOX_COMBAT_EVENTS_DELAY,
	CHECKBOX_COMBAT_EVENTS_TACTICAL_WITHDRAWAL,
	CHECKBOX_COMBAT_EVENTS_BREAKTHROUGH,
	CHECKBOX_COMBAT_EVENTS_HQ_BONUS_CHANCE,

	END_CHECKBOX_COMBAT_EVENTS_ENUM,
	
	CHECKBOX_MISC_MILITARY_ARMIES_CAN_DIG_IN = END_CHECKBOX_COMBAT_EVENTS_ENUM,
	CHECKBOX_MISC_MILITARY_MISSILES_CAN_CARRY_NUKES,
	CHECKBOX_MISC_MILITARY_PROVINCIAL_AA_EFFICIENCY,
	CHECKBOX_MISC_MILITARY_ATTRITION_MODIFIER,
	CHECKBOX_MISC_MILITARY_HQ_ESE_BONUS,
	CHECKBOX_MISC_MILITARY_GROUND_DEFENSE_EFFICIENCY,
	CHECKBOX_MISC_MILITARY_CONVOY_DEFENSE_EFFICIENCY,
	CHECKBOX_MISC_MILITARY_LAND_FORT_EFFICIENCY,
	CHECKBOX_MISC_MILITARY_COAST_FORT_EFFICIENCY,

	END_CHECKBOX_MISC_MILITARY_ENUM,
	
	CHECKBOX_INTEL_AND_RESEARCH_FRIENDLY_INTEL_CHANCE = END_CHECKBOX_MISC_MILITARY_ENUM,
	CHECKBOX_INTEL_AND_RESEARCH_ENEMY_INTEL_CHANCE,
	CHECKBOX_INTEL_AND_RESEARCH_RADAR_EFFICIENCY,
	CHECKBOX_INTEL_AND_RESEARCH_RESEARCH_SPEED,

	END_CHECKBOX_TECHNOLOGY_OVERVIEW_ENUM
};
//=============================================================================================================