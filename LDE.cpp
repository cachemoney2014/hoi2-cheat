//=============================================================================================================
#include "Required.h"
//=============================================================================================================
#include "LDE.h"
//=============================================================================================================
int LDEDisasm ( _Inout_ LDEDisasm_t* pDisasm, _In_ unsigned char* Address, _In_ int Mode )
{
	bool FinishedPrefixes = false;

	unsigned char* CurrentPointer = Address, CurrentValue = 0;

	int Length = 0;

	pDisasm->m_CurrentPointer = ( uintptr_t )Address;

	for ( int Iterator = MAX_INSTRUCTION_SIZE; Iterator > 0, FinishedPrefixes != true; Iterator-- ) // gather prefixes
	{
		switch ( CurrentValue = *CurrentPointer++ )
		{
			case PREFIX_SEGMENT_CS:
			case PREFIX_SEGMENT_SS:
			case PREFIX_SEGMENT_DS:
			case PREFIX_SEGMENT_ES:
			case PREFIX_SEGMENT_FS:
			case PREFIX_SEGMENT_GS:
			{
				pDisasm->m_PrefixSegment = CurrentValue;

				if ( CurrentValue == PREFIX_SEGMENT_ES )
				{
					pDisasm->m_Prefix26 = PREFIX_SEGMENT_ES;
				}
				else if ( CurrentValue == PREFIX_SEGMENT_CS )
				{
					pDisasm->m_Prefix2E = PREFIX_SEGMENT_DS;
				}
				else if ( CurrentValue == PREFIX_SEGMENT_SS )
				{
					pDisasm->m_Prefix36 = PREFIX_SEGMENT_SS;
				}
				else if ( CurrentValue == PREFIX_SEGMENT_DS )
				{
					pDisasm->m_Prefix3E = PREFIX_SEGMENT_DS;
				}
				else if ( CurrentValue == PREFIX_SEGMENT_FS )
				{
					pDisasm->m_Prefix64 = PREFIX_SEGMENT_FS;
				}
				else if ( CurrentValue == PREFIX_SEGMENT_GS )
				{
					pDisasm->m_Prefix65 = PREFIX_SEGMENT_GS;
				}

				pDisasm->m_Flags |= FLAG_PREFIX_SEGMENT;
			}
			break;

			case PREFIX_OPERAND_SIZE:
			{
				pDisasm->m_Prefix66 = pDisasm->m_PrefixOperandSizeOverride = CurrentValue;

				pDisasm->m_Flags |= FLAG_PREFIX_OVERRIDE_SIZE;
			}
			break;

			case PREFIX_ADDRESS_SIZE:
			{
				pDisasm->m_Prefix67 = pDisasm->m_PrefixAddressSizeOverride = CurrentValue;

				pDisasm->m_Flags |= FLAG_PREFIX_OVERRIDE_ADDRESS;
			}
			break;

			case PREFIX_LOCK:
			{
				pDisasm->m_PrefixLock = CurrentValue;

				pDisasm->m_Flags |= FLAG_PREFIX_LOCK;
			}
			break;

			case PREFIX_REPX:
			case PREFIX_REPNZ:
			{
				pDisasm->m_PrefixRepeat = CurrentValue;

				if ( CurrentValue == PREFIX_REPNZ )
				{
					pDisasm->m_PrefixF2 = CurrentValue;
				}
				else
				{
					pDisasm->m_PrefixF3 = CurrentValue;
				}

				pDisasm->m_Flags |= FLAG_PREFIX_REPEAT;
			}
			break;

			case PREFIX_WAIT:
			{
				pDisasm->m_PrefixWait = CurrentValue;

				pDisasm->m_Prefix9B = CurrentValue;

				pDisasm->m_Flags |= FLAG_PREFIX_WAIT;
			}
			break;

			default:
			{
				FinishedPrefixes = true;
			}
			break;
		}
	}

	if ( Mode == MODE_32 )
	{
		LDEDisasm32 ( pDisasm, CurrentPointer, CurrentValue );
	}
	else if ( Mode == MODE_64 )
	{
		if ( ( *CurrentPointer & 0xF0 ) == PREFIX_REX )
		{
			pDisasm->m_REXPrefix = *CurrentPointer++;

			pDisasm->m_REX_W = ( ( pDisasm->m_REXPrefix & 0xF ) >> 3 );
			pDisasm->m_REX_R = ( ( pDisasm->m_REXPrefix & 7 ) >> 2 );
			pDisasm->m_REX_X = ( ( pDisasm->m_REXPrefix & 3 ) >> 1 );
			pDisasm->m_REX_B = pDisasm->m_REXPrefix & 1;

			pDisasm->m_Flags |= FLAG_PREFIX_REX;
		}

		LDEDisasm64 ( pDisasm, CurrentPointer, CurrentValue );
	}

	if ( pDisasm->m_Flags & FLAG_REL8 )
	{
		pDisasm->Relative.Relative32 = ( signed char )*CurrentPointer++;
	}
	else if ( pDisasm->m_Flags & FLAG_REL16 )
	{
		pDisasm->Relative.Relative32 = *( signed short* )CurrentPointer;

		CurrentPointer += sizeof ( unsigned short );
	}
	else if ( pDisasm->m_Flags & FLAG_REL32 )
	{
		pDisasm->Relative.Relative32 = *( signed long* )CurrentPointer;

		CurrentPointer += sizeof ( unsigned long );
	}
	if ( pDisasm->m_Flags & FLAG_SEGMENT_REL )
	{
		pDisasm->m_Segment = *( unsigned short* )CurrentPointer;

		CurrentPointer += sizeof ( unsigned short );
	}

	if ( pDisasm->m_Flags & FLAG_IMM8 )
	{
		pDisasm->Immediate.Immediate64 = *CurrentPointer++;
	}
	else if ( pDisasm->m_Flags & FLAG_IMM16 )
	{
		pDisasm->Immediate.Immediate64 = *( unsigned short* )CurrentPointer;

		CurrentPointer += sizeof ( unsigned short );
	}
	else if ( pDisasm->m_Flags & FLAG_IMM32 )
	{
		pDisasm->Immediate.Immediate64 = *( unsigned long* )CurrentPointer;

		CurrentPointer += sizeof ( unsigned long );
	}
	else if ( pDisasm->m_Flags & FLAG_IMM64 )
	{
		pDisasm->Immediate.Immediate64 = *( unsigned long long* )CurrentPointer;

		CurrentPointer += sizeof ( unsigned long long );
	}
	if ( pDisasm->m_Flags & FLAG_IMM8_2 )
	{
		pDisasm->Immediate2.Immediate64 = *CurrentPointer++;
	}

	if ( pDisasm->m_Flags & FLAG_OPCEXT )
	{
		pDisasm->m_Flags |= FLAG_MODRM;
	}

	Length = CurrentPointer - Address;

	pDisasm->m_Length = Length;
		
	return Length;
}
//=============================================================================================================
void LDEDisasm32 ( LDEDisasm_t* pDisasm, unsigned char*& CurrentPointer, unsigned char CurrentValue )
{
	bool ProcessModRM;

	if ( CurrentValue == OPCODE_TWO_BYTE )
	{
		CurrentValue = *CurrentPointer++;

		pDisasm->m_Flags |= FLAG_PREFIX_0F;

		if ( CurrentValue == PREFIX_3DNOW )
		{
			pDisasm->m_Flags |= FLAG_3DNOW;

			LDEModRMAndSIB3264 ( pDisasm, CurrentPointer );

			// check last byte for opcode

			pDisasm->m_Opcode = *CurrentPointer++;
		}
		else
		{
			pDisasm->m_Opcode = CurrentValue;

			switch ( CurrentValue )
			{
				case 0x00:
				case 0x01:
				case 0x18:
				case 0x19:
				case 0x1A:
				case 0x1B:
				case 0x1C:
				case 0x1D:
				case 0x1E:
				case 0x1F:
				case 0x71:
				case 0x72:
				case 0x73:
				case 0x90:
				case 0x91:
				case 0x92:
				case 0x93:
				case 0x94:
				case 0x95:
				case 0x96:
				case 0x97:
				case 0x98:
				case 0x99:
				case 0x9A:
				case 0x9B:
				case 0x9C:
				case 0x9D:
				case 0x9E:
				case 0x9F:
				case 0xAE:
				case 0xBA:
				case 0xC7:
				{
					if ( CurrentValue >= 0x71 && CurrentValue <= 0x73 )
					{
						pDisasm->m_Flags |= FLAG_IMM8;
					}

					pDisasm->m_Flags |= FLAG_OPCEXT;
				}
				break;

				case 0x02:
				case 0x03:
				case 0x10:
				case 0x11:
				case 0x12:
				case 0x13:
				case 0x14:
				case 0x15:
				case 0x16:
				case 0x17:
				case 0x20:
				case 0x21:
				case 0x22:
				case 0x23:
				case 0x24:
				case 0x26:
				case 0x27:
				case 0x28:
				case 0x29:
				case 0x2A:
				case 0x2B:
				case 0x2C:
				case 0x2D:
				case 0x2E:
				case 0x2F:
				case 0x38:
				case 0x3A:
				case 0x40:
				case 0x41:
				case 0x42:
				case 0x43:
				case 0x44:
				case 0x45:
				case 0x46:
				case 0x47:
				case 0x48:
				case 0x49:
				case 0x4A:
				case 0x4B:
				case 0x4C:
				case 0x4D:
				case 0x4E:
				case 0x4F:
				case 0x50:
				case 0x51:
				case 0x52:
				case 0x53:
				case 0x54:
				case 0x55:
				case 0x56:
				case 0x57:
				case 0x58:
				case 0x59:
				case 0x5A:
				case 0x5B:
				case 0x5C:
				case 0x5D:
				case 0x5E:
				case 0x5F:
				case 0x60:
				case 0x61:
				case 0x62:
				case 0x63:
				case 0x64:
				case 0x65:
				case 0x66:
				case 0x67:
				case 0x68:
				case 0x69:
				case 0x6A:
				case 0x6B:
				case 0x6C:
				case 0x6D:
				case 0x6E:
				case 0x6F:
				case 0x70:
				case 0x74:
				case 0x75:
				case 0x76:
				case 0x78:
				case 0x79:
				case 0x7C:
				case 0x7D:
				case 0x7E:
				case 0x7F:
				case 0xA3:
				case 0xA4:
				case 0xA5:
				case 0xAB:
				case 0xAC:
				case 0xAD:
				case 0xAF:
				case 0xB0:
				case 0xB1:
				case 0xB2:
				case 0xB3:
				case 0xB4:
				case 0xB5:
				case 0xB6:
				case 0xB7:
				case 0xB8:
				case 0xBB:
				case 0xBC:
				case 0xBD:
				case 0xBE:
				case 0xBF:
				case 0xC0:
				case 0xC1:
				case 0xC2:
				case 0xC3:
				case 0xC4:
				case 0xC5:
				case 0xC6:
				case 0xD0:
				case 0xD1:
				case 0xD2:
				case 0xD3:
				case 0xD4:
				case 0xD5:
				case 0xD6:
				case 0xD7:
				case 0xD8:
				case 0xD9:
				case 0xDA:
				case 0xDB:
				case 0xDC:
				case 0xDD:
				case 0xDE:
				case 0xDF:
				case 0xE0:
				case 0xE1:
				case 0xE2:
				case 0xE3:
				case 0xE4:
				case 0xE5:
				case 0xE6:
				case 0xE7:
				case 0xE8:
				case 0xE9:
				case 0xEA:
				case 0xEB:
				case 0xEC:
				case 0xED:
				case 0xEE:
				case 0xEF:
				case 0xF0:
				case 0xF1:
				case 0xF2:
				case 0xF3:
				case 0xF4:
				case 0xF5:
				case 0xF6:
				case 0xF7:
				case 0xF8:
				case 0xF9:
				case 0xFA:
				case 0xFB:
				case 0xFC:
				case 0xFD:
				case 0xFE:
				{
					ProcessModRM = true;

					switch ( CurrentValue )
					{
						case 0x38:
						case 0x3A:
						{
							pDisasm->m_Opcode2 = *CurrentPointer++;
												
							if ( CurrentValue == 0x3A )
							{
								if ( pDisasm->m_Opcode2 >= 0x08 && pDisasm->m_Opcode2 <= 0x0F 
									|| pDisasm->m_Opcode2 >= 0x14 && pDisasm->m_Opcode2 <= 0x22
									|| pDisasm->m_Opcode2 == 0x42
									|| pDisasm->m_Opcode2 == 0x62
									|| pDisasm->m_Opcode2 == 0x63 )
								{
									pDisasm->m_Flags |= FLAG_IMM8;
								}
							}
						}
						break;

						case 0x70:
						{
							pDisasm->m_Flags |= FLAG_IMM8;
						}
						break;

						case 0xA4:
						case 0xA5:
						{							
							pDisasm->m_HasDirectionFlag = true;

							if ( CurrentValue == 0xA4 )
							{
								pDisasm->m_HasImmediate = true;

								pDisasm->m_Flags |= FLAG_IMM8;
							}
						}
						break;

						case 0xAB:
						{
							pDisasm->m_IsLockPrefixValid = true;
						}
						break;

						case 0xAC:
						case 0xAD:
						{
							if ( CurrentValue == 0xAC )
							{
								pDisasm->m_Flags |= FLAG_IMM8;
							}

							pDisasm->m_HasDirectionFlag = true;
						}
						break;
						
						case 0xB0: 
						case 0xB1:
						{
							pDisasm->m_IsPrefixOperandSizeOverideValid = true;

							pDisasm->m_IsLockPrefixValid = true;
						}
						break;

						case 0xB2:
						case 0xB4:
						case 0xB5:
						{				
							pDisasm->m_Flags |= FLAG_SEGMENT_REGISTER;
						}
						break;

						case 0xAF:
						case 0xB6:
						case 0xB7:
						case 0xBE:
						case 0xBF:
						{
							pDisasm->m_IsPrefixOperandSizeOverideValid = true;
							pDisasm->m_HasDirectionFlag = true;
						}
						break;

						case 0xB8:
						{
							if ( pDisasm->m_PrefixRepeat != PREFIX_REPX )
							{
								ProcessModRM = false;
							}
						}
						break;

						case 0xC0:
						case 0xC1:
						{
							pDisasm->m_IsLockPrefixValid = true;
							pDisasm->m_HasDirectionFlag = true;
						}
						break;

						case 0xC2:
						case 0xC3:
						case 0xC4:
						case 0xC5:
						case 0xC6:
						{
							pDisasm->m_Flags |= FLAG_IMM8;
						}
						break;
					}
					if ( ProcessModRM == true )
					{						
						pDisasm->m_Flags |= FLAG_MODRM;
					}
				}
				break;
					
				case 0x80:
				case 0x81:
				case 0x82:
				case 0x83:
				case 0x84:
				case 0x85:
				case 0x86:
				case 0x87:
				case 0x88:
				case 0x89:
				case 0x8A:
				case 0x8B:
				case 0x8C:
				case 0x8D:
				case 0x8E:
				case 0x8F:
				{
					pDisasm->m_HasRelative = true;

					pDisasm->m_IsPrefixOperandSizeOverideValid = true;
				
					if ( pDisasm->m_PrefixOperandSizeOverride )
					{				
						pDisasm->m_Flags |= FLAG_REL16;
					}
					else
					{				
						pDisasm->m_Flags |= FLAG_REL32;
					}
				}
				break;
				
				case 0xA0:
				case 0xA1:
				case 0xA8:
				case 0xA9:
				{
					pDisasm->m_Flags |= FLAG_SEGMENT_REGISTER;
				}
				break;
				
				case 0xC8:
				case 0xC9:
				case 0xCA:
				case 0xCB:
				case 0xCC:
				case 0xCD:
				case 0xCE:
				case 0xCF:
				{
					pDisasm->m_Register = CurrentValue & 0x07;
					pDisasm->m_IsRegisterInOpcode = true;
				}
				break;
			}
		}
	}
	else
	{
		pDisasm->m_Opcode = CurrentValue;

		switch ( CurrentValue )
		{
			case 0x00:
			case 0x01:
			case 0x02:
			case 0x03:
			case 0x08:
			case 0x09:
			case 0x0A:
			case 0x0B:
			case 0x10:
			case 0x11:
			case 0x12:
			case 0x13:
			case 0x18:
			case 0x19:
			case 0x1A:
			case 0x1B:
			case 0x20:
			case 0x21:
			case 0x22:
			case 0x23:
			case 0x28:
			case 0x29:
			case 0x2A:
			case 0x2B:
			case 0x30:
			case 0x31:
			case 0x32:
			case 0x33:
			case 0x38:
			case 0x39:
			case 0x3A:
			case 0x3B:
			{				
				switch ( CurrentValue & 0x07 )
				{
					case 0x00:
					case 0x01:
					{
						pDisasm->m_IsLockPrefixValid = true;
					}
					break;
				}

				pDisasm->m_HasDirectionFlag = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				pDisasm->m_Flags |= FLAG_MODRM;
			}
			break;


			case 0x04:
			case 0x05:
			case 0x0C:
			case 0x0D:
			case 0x14:
			case 0x15:
			case 0x1C:
			case 0x1D:
			case 0x24:
			case 0x25:
			case 0x2C:
			case 0x2D:
			case 0x34:
			case 0x35:
			case 0x3C:
			case 0x3D:
			{
				pDisasm->m_HasImmediate = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				if ( ( CurrentValue & 0x07 ) == 5 )
				{
					if ( pDisasm->m_PrefixOperandSizeOverride )
					{
						pDisasm->m_Flags |= FLAG_IMM16;
					}
					else
					{
						pDisasm->m_Flags |= FLAG_IMM32;
					}
				}
				else
				{
					pDisasm->m_Flags |= FLAG_IMM8;
				}
			}
			break;

			case 0x06:
			case 0x07:
			case 0x0E:
			case 0x0F:
			case 0x16:
			case 0x17:
			case 0x1E:
			case 0x1F:
			{
				pDisasm->m_Flags |= FLAG_SEGMENT_REGISTER;
			}
			break;
			
			case 0x40:
			case 0x41:
			case 0x42:
			case 0x43:
			case 0x44:
			case 0x45:
			case 0x46:
			case 0x47:
			case 0x48:
			case 0x49:
			case 0x4A:
			case 0x4B:
			case 0x4C:
			case 0x4D:
			case 0x4E:
			case 0x4F:
			case 0x50:
			case 0x51:
			case 0x52:
			case 0x53:
			case 0x54:
			case 0x55:
			case 0x56:
			case 0x57:
			case 0x58:
			case 0x59:
			case 0x5A:
			case 0x5B:
			case 0x5C:
			case 0x5D:
			case 0x5E:
			case 0x5F:
			{
				pDisasm->m_Register = CurrentValue & 0x07;
				pDisasm->m_IsRegisterInOpcode = true;
			}
			break;

			case 0x60:
			case 0x61:
			{
				pDisasm->m_Register = ALL_GENERAL_PURPOSE;
			}
			break;

			case 0x62:
			case 0x63:
			{
				pDisasm->m_Flags |= FLAG_MODRM;
			}
			break;

			case 0x68:
			case 0x69:
			{
				pDisasm->m_HasImmediate = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				if ( pDisasm->m_PrefixOperandSizeOverride  )
				{
					pDisasm->m_Flags |= FLAG_IMM16;
				}
				else
				{
					pDisasm->m_Flags |= FLAG_IMM32;
				}

				if ( CurrentValue == 0x69 )
				{
					pDisasm->m_Flags |= FLAG_MODRM;
				}
			}
			break;

			case 0x6A:
			case 0x6B:
			{
				if ( CurrentValue == 0x6B )
				{
					pDisasm->m_HasImmediate = true;

					pDisasm->m_Flags |= FLAG_MODRM;
				}
				
				pDisasm->m_Flags |= FLAG_IMM8;
			}
			break;

			case 0x70:
			case 0x71:
			case 0x72:
			case 0x73:
			case 0x74:
			case 0x75:
			case 0x76:
			case 0x77:
			case 0x78:
			case 0x79:
			case 0x7A:
			case 0x7B:
			case 0x7C:
			case 0x7D:
			case 0x7E:
			case 0x7F:
			{
				pDisasm->m_HasRelative = true;
						
				pDisasm->m_Flags |= FLAG_REL8;
			}
			break;
			
			case 0x80:
			case 0x81:
			case 0x82:
			case 0x83:
			{
				pDisasm->m_HasImmediate = true;

				if ( CurrentValue != 0x81 )
				{					
					pDisasm->m_Flags |= FLAG_IMM8;
				}
				else
				{
					pDisasm->m_IsPrefixOperandSizeOverideValid = true;

					if ( pDisasm->m_PrefixOperandSizeOverride  )
					{				
						pDisasm->m_Flags |= FLAG_IMM16;
					}
					else
					{			
						pDisasm->m_Flags |= FLAG_IMM32;
					}
				}
								
				pDisasm->m_Flags |= FLAG_OPCEXT;
			}
			break;

			case 0x84:
			case 0x85:
			case 0x86:
			case 0x87:
			case 0x88:
			case 0x89:
			case 0x8A:
			case 0x8B:
			{
				pDisasm->m_HasDirectionFlag = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				pDisasm->m_Flags |= FLAG_MODRM;
			}
			break;

			case 0x8C:
			case 0x8D:
			case 0x8E:
			{
				if ( CurrentValue == 0x8C || CurrentValue == 0x8E )
				{
					pDisasm->m_HasDirectionFlag = true;
				}

				pDisasm->m_Flags |= FLAG_MODRM;
			}
			break;

			case 0x8F:
			{
				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				pDisasm->m_Flags |= FLAG_OPCEXT;
			}
			break;

			case 0x90:
			case 0x91:
			case 0x92:
			case 0x93:
			case 0x94:
			case 0x95:
			case 0x96:
			case 0x97:
			case 0x98:
			case 0x99:
			{
				if ( CurrentValue == 0x90 )
				{
					pDisasm->m_IsPrefixRepeatValid = true;
				}

				pDisasm->m_Register = CurrentValue & 0x07;
				pDisasm->m_IsRegisterInOpcode = true;
			}
			break;

			case 0x9A:
			{
				pDisasm->m_HasRelative = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;
				
				if ( pDisasm->m_PrefixOperandSizeOverride  )
				{				
					pDisasm->m_Flags |= FLAG_REL16;
				}
				else
				{				
					pDisasm->m_Flags |= FLAG_REL32;
				}

				pDisasm->m_Flags |= FLAG_SEGMENT_REL;
			}
			break;
			
			case 0xA0:
			case 0xA1:
			case 0xA2:
			case 0xA3:
			{
				pDisasm->m_HasDisplacement = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				pDisasm->Displacement.Displacement32 = *( unsigned long* )CurrentPointer;

				CurrentPointer += sizeof ( unsigned long );

				pDisasm->m_Flags |= FLAG_DISP32;
			}
			break;

			case 0xA8:
			{
				pDisasm->m_Flags |= FLAG_IMM8;
			}
			break;

			case 0xA9:
			{
				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				if ( pDisasm->m_PrefixOperandSizeOverride  )
				{				
					pDisasm->m_Flags |= FLAG_IMM16;
				}
				else
				{			
					pDisasm->m_Flags |= FLAG_IMM32;
				}
			}
			break;
			
			case 0xB0:
			case 0xB1:
			case 0xB2:
			case 0xB3:
			case 0xB4:
			case 0xB5:
			case 0xB6:
			case 0xB7:
			case 0xB8:
			case 0xB9:
			case 0xBA:
			case 0xBB:
			case 0xBC:
			case 0xBD:
			case 0xBE:
			case 0xBF:
			{
				pDisasm->m_HasImmediate = true;

				pDisasm->m_Register = CurrentValue & 0x07;
				pDisasm->m_IsRegisterInOpcode = true;

				if ( CurrentValue < 0xB8 )
				{
					pDisasm->m_Flags |= FLAG_IMM8;
				}
				else
				{
					pDisasm->m_Register = CurrentValue & 0x07;

					pDisasm->m_IsPrefixOperandSizeOverideValid = true;

					if ( pDisasm->m_PrefixOperandSizeOverride )
					{
						pDisasm->m_Flags |= FLAG_IMM16;
					}
					else
					{
						pDisasm->m_Flags |= FLAG_IMM32;
					}
				}
			}
			break;

			case 0xC0:
			case 0xC1:
			{
				pDisasm->m_HasImmediate = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				pDisasm->m_Flags |= FLAG_OPCEXT | FLAG_IMM8;
			}
			break;

			case 0xC2:
			{
				pDisasm->m_HasImmediate = true;

				pDisasm->m_Flags |= FLAG_IMM16;
			}
			break;
			
			case 0xC4:
			case 0xC5:
			{
				pDisasm->m_Flags |= FLAG_MODRM;
			}
			break;

			case 0xC6:
			case 0xC7:
			{					
				pDisasm->m_Flags |= FLAG_OPCEXT;
			}
			break;
			
			case 0xC8:
			case 0xCA:
			case 0xCD:
			{
				pDisasm->m_HasImmediate = true;

				switch ( CurrentValue )
				{
					case 0xC8:
					{
						pDisasm->m_Flags |= FLAG_IMM16 | FLAG_IMM8_2;
					}
					break;

					case 0xCA:
					{
						pDisasm->m_Flags |= FLAG_IMM16;
					}
					break;

					case 0xCD:
					{
						pDisasm->m_Flags |= FLAG_IMM8;
					}
					break;
				}
			}
			break;
			
			case 0xD0:
			case 0xD1:
			case 0xD2:
			case 0xD3:
			{
				pDisasm->m_IsPrefixOperandSizeOverideValid = true;
				
				pDisasm->m_Flags |= FLAG_OPCEXT;
			}
			break;
			
			case 0xD4:
			case 0xD5:
			{
				if ( *( CurrentPointer + 1 ) == 0x0A )
				{
					pDisasm->m_Opcode2 = *CurrentPointer++;
				}
				else
				{
					pDisasm->m_HasImmediate = true;

					pDisasm->m_Flags |= FLAG_IMM8;
				}
			}
			break;
			
			case 0xE0:
			case 0xE1:
			case 0xE2:
			case 0xE3:
			{
				pDisasm->m_HasRelative = true;

				pDisasm->m_Flags |= FLAG_REL8;
			}
			break;

			case 0xE4:
			case 0xE5:
			case 0xE6:
			case 0xE7:
			{
				pDisasm->m_HasImmediate = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				pDisasm->m_Flags |= FLAG_IMM8;
			}
			break;
			
			case 0xE8:
			case 0xE9:
			case 0xEA:
			case 0xEB:
			{
				pDisasm->m_HasRelative = true;

				switch ( CurrentValue )
				{
					case 0xE8:
					case 0xE9:
					case 0xEA:
					{
						pDisasm->m_IsPrefixOperandSizeOverideValid = true;

						if ( pDisasm->m_PrefixOperandSizeOverride )
						{				
							pDisasm->m_Flags |= FLAG_REL16;
						}
						else
						{				
							pDisasm->m_Flags |= FLAG_REL32;
						}

						if ( CurrentValue == 0xEA )
						{
							pDisasm->m_Flags |= FLAG_SEGMENT_REL;
						}
					}
					break;

					case 0xEB:
					{
						pDisasm->m_Flags |= FLAG_REL8;
					}
					break;
				}
			}
			break;

			case 0xD8:
			case 0xD9:
			case 0xDA:
			case 0xDB:
			case 0xDC:
			case 0xDD:
			case 0xDE:
			case 0xDF:
			case 0xF6:
			case 0xF7:
			case 0xFE:
			case 0xFF:
			{
				if ( CurrentValue == 0xF6 )
				{
					pDisasm->m_IsPrefixOperandSizeOverideValid = true;
				}
				
				pDisasm->m_Flags |= FLAG_OPCEXT;
			}
			break;
		}
	}

	if ( !( pDisasm->m_Flags & FLAG_3DNOW ) && pDisasm->m_Flags & FLAG_MODRM || pDisasm->m_Flags & FLAG_OPCEXT )
	{
		LDEModRMAndSIB3264 ( pDisasm, CurrentPointer );
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_0F )
	{
		switch ( CurrentValue )
		{	
			case 0xBA:
			{
				if ( pDisasm->m_ModRM_Reg > 4 )
				{
					pDisasm->m_IsLockPrefixValid = true;
				}

				if ( pDisasm->m_ModRM_Reg > 3 )
				{
					pDisasm->m_HasImmediate = true;

					pDisasm->m_Flags |= FLAG_IMM8;
				}
			}
			break;
		}
	}
	else
	{
		switch ( CurrentValue )
		{
			case 0x80:
			case 0x81:
			case 0x82:
			case 0x83:
			{
				if ( pDisasm->m_ModRM_Reg != 7 )
				{
					pDisasm->m_IsLockPrefixValid = true;
				}
			}
			break;

			case 0xC6:
			case 0xC7:
			{
				if ( pDisasm->m_ModRM_Reg == 0 )
				{
					pDisasm->m_HasImmediate = true;

					pDisasm->m_IsPrefixOperandSizeOverideValid = true;

					if ( CurrentValue == 0xC7 )
					{
						if ( pDisasm->m_PrefixOperandSizeOverride )
						{
							pDisasm->m_Flags |= FLAG_IMM16;
						}
						else
						{
							pDisasm->m_Flags |= FLAG_IMM32;
						}
					}
					else
					{
						pDisasm->m_Flags |= FLAG_IMM8;
					}
				}
			}
			break;

			case 0xF6:
			{
				if ( pDisasm->m_ModRM_Reg <= 1 )
				{
					pDisasm->m_HasImmediate = true;

					pDisasm->m_Flags |= FLAG_IMM8;
				}
			}
			break;

			case 0xF7:
			{
				if ( pDisasm->m_ModRM_Reg <= 4 )
				{
					pDisasm->m_IsPrefixOperandSizeOverideValid = true;
				}
				if ( pDisasm->m_ModRM_Reg <= 1 )
				{
					pDisasm->m_HasImmediate = true;

					if ( pDisasm->m_PrefixOperandSizeOverride  )
					{				
						pDisasm->m_Flags |= FLAG_IMM16;
					}
					else
					{				
						pDisasm->m_Flags |= FLAG_IMM32;
					}
				}
			}
			break;
		}
	}
}
//=============================================================================================================
void LDEDisasm64 ( _Inout_ LDEDisasm_t* pDisasm, _Inout_ unsigned char*& CurrentPointer, _In_ unsigned char CurrentValue )
{
	bool ProcessModRM = true;

	if ( CurrentValue == OPCODE_TWO_BYTE )
	{
		CurrentValue = *CurrentPointer++;

		pDisasm->m_Flags |= FLAG_PREFIX_0F;

		if ( CurrentValue == PREFIX_3DNOW )
		{
			pDisasm->m_Flags |= FLAG_3DNOW;

			LDEModRMAndSIB3264 ( pDisasm, CurrentPointer );

			// check last byte for opcode

			pDisasm->m_Opcode = *CurrentPointer++;
		}
		else
		{
			pDisasm->m_Opcode = CurrentValue;

			switch ( CurrentValue )
			{
				case 0x00:
				case 0x01:
				case 0x18:
				case 0x19:
				case 0x1A:
				case 0x1B:
				case 0x1C:
				case 0x1D:
				case 0x1E:
				case 0x1F:
				case 0x71:
				case 0x72:
				case 0x73:
				case 0x90:
				case 0x91:
				case 0x92:
				case 0x93:
				case 0x94:
				case 0x95:
				case 0x96:
				case 0x97:
				case 0x98:
				case 0x99:
				case 0x9A:
				case 0x9B:
				case 0x9C:
				case 0x9D:
				case 0x9E:
				case 0x9F:
				case 0xAE:
				case 0xBA:
				case 0xC7:
				{
					if ( CurrentValue >= 0x71 && CurrentValue <= 0x73 )
					{
						pDisasm->m_Flags |= FLAG_IMM8;
					}

					pDisasm->m_Flags |= FLAG_OPCEXT;
				}
				break;

				case 0x02:
				case 0x03:
				case 0x10:
				case 0x11:
				case 0x12:
				case 0x13:
				case 0x14:
				case 0x15:
				case 0x16:
				case 0x17:
				case 0x20:
				case 0x21:
				case 0x22:
				case 0x23:
				case 0x24:
				case 0x26:
				case 0x27:
				case 0x28:
				case 0x29:
				case 0x2A:
				case 0x2B:
				case 0x2C:
				case 0x2D:
				case 0x2E:
				case 0x2F:
				case 0x38:
				case 0x3A:
				case 0x40:
				case 0x41:
				case 0x42:
				case 0x43:
				case 0x44:
				case 0x45:
				case 0x46:
				case 0x47:
				case 0x48:
				case 0x49:
				case 0x4A:
				case 0x4B:
				case 0x4C:
				case 0x4D:
				case 0x4E:
				case 0x4F:
				case 0x50:
				case 0x51:
				case 0x52:
				case 0x53:
				case 0x54:
				case 0x55:
				case 0x56:
				case 0x57:
				case 0x58:
				case 0x59:
				case 0x5A:
				case 0x5B:
				case 0x5C:
				case 0x5D:
				case 0x5E:
				case 0x5F:
				case 0x60:
				case 0x61:
				case 0x62:
				case 0x63:
				case 0x64:
				case 0x65:
				case 0x66:
				case 0x67:
				case 0x68:
				case 0x69:
				case 0x6A:
				case 0x6B:
				case 0x6C:
				case 0x6D:
				case 0x6E:
				case 0x6F:
				case 0x70:
				case 0x74:
				case 0x75:
				case 0x76:
				case 0x78:
				case 0x79:
				case 0x7C:
				case 0x7D:
				case 0x7E:
				case 0x7F:
				case 0xA3:
				case 0xA4:
				case 0xA5:
				case 0xAB:
				case 0xAC:
				case 0xAD:
				case 0xAF:
				case 0xB0:
				case 0xB1:
				case 0xB2:
				case 0xB3:
				case 0xB4:
				case 0xB5:
				case 0xB6:
				case 0xB7:
				case 0xB8:
				case 0xBB:
				case 0xBC:
				case 0xBD:
				case 0xBE:
				case 0xBF:
				case 0xC0:
				case 0xC1:
				case 0xC2:
				case 0xC3:
				case 0xC4:
				case 0xC5:
				case 0xC6:
				case 0xD0:
				case 0xD1:
				case 0xD2:
				case 0xD3:
				case 0xD4:
				case 0xD5:
				case 0xD6:
				case 0xD7:
				case 0xD8:
				case 0xD9:
				case 0xDA:
				case 0xDB:
				case 0xDC:
				case 0xDD:
				case 0xDE:
				case 0xDF:
				case 0xE0:
				case 0xE1:
				case 0xE2:
				case 0xE3:
				case 0xE4:
				case 0xE5:
				case 0xE6:
				case 0xE7:
				case 0xE8:
				case 0xE9:
				case 0xEA:
				case 0xEB:
				case 0xEC:
				case 0xED:
				case 0xEE:
				case 0xEF:
				case 0xF0:
				case 0xF1:
				case 0xF2:
				case 0xF3:
				case 0xF4:
				case 0xF5:
				case 0xF6:
				case 0xF7:
				case 0xF8:
				case 0xF9:
				case 0xFA:
				case 0xFB:
				case 0xFC:
				case 0xFD:
				case 0xFE:
				{
					ProcessModRM = true;

					switch ( CurrentValue )
					{
						case 0x38:
						case 0x3A:
						{
							pDisasm->m_Opcode2 = *CurrentPointer++;
												
							if ( CurrentValue == 0x3A )
							{
								if ( pDisasm->m_Opcode2 >= 0x08 && pDisasm->m_Opcode2 <= 0x0F 
									|| pDisasm->m_Opcode2 >= 0x14 && pDisasm->m_Opcode2 <= 0x22
									|| pDisasm->m_Opcode2 == 0x42
									|| pDisasm->m_Opcode2 == 0x62
									|| pDisasm->m_Opcode2 == 0x63 )
								{
									pDisasm->m_Flags |= FLAG_IMM8;
								}
							}
						}
						break;

						case 0x70:
						{
							pDisasm->m_Flags |= FLAG_IMM8;
						}
						break;

						case 0xA4:
						case 0xA5:
						{							
							pDisasm->m_HasDirectionFlag = true;

							if ( CurrentValue == 0xA4 )
							{
								pDisasm->m_HasImmediate = true;

								pDisasm->m_Flags |= FLAG_IMM8;
							}
						}
						break;

						case 0xAB:
						{
							pDisasm->m_IsLockPrefixValid = true;
						}
						break;

						case 0xAC:
						case 0xAD:
						{
							if ( CurrentValue == 0xAC )
							{
								pDisasm->m_Flags |= FLAG_IMM8;
							}

							pDisasm->m_HasDirectionFlag = true;
						}
						break;
						
						case 0xB0: 
						case 0xB1:
						{
							pDisasm->m_IsPrefixOperandSizeOverideValid = true;

							pDisasm->m_IsLockPrefixValid = true;
						}
						break;

						case 0xB2:
						case 0xB4:
						case 0xB5:
						{				
							pDisasm->m_Flags |= FLAG_SEGMENT_REGISTER;
						}
						break;

						case 0xAF:
						case 0xB6:
						case 0xB7:
						case 0xBE:
						case 0xBF:
						{
							pDisasm->m_IsPrefixOperandSizeOverideValid = true;
							pDisasm->m_HasDirectionFlag = true;
						}
						break;

						case 0xB8:
						{
							if ( pDisasm->m_PrefixRepeat != PREFIX_REPX )
							{
								ProcessModRM = false;
							}
						}
						break;

						case 0xC0:
						case 0xC1:
						{
							pDisasm->m_IsLockPrefixValid = true;
							pDisasm->m_HasDirectionFlag = true;
						}
						break;

						case 0xC2:
						case 0xC3:
						case 0xC4:
						case 0xC5:
						case 0xC6:
						{
							pDisasm->m_Flags |= FLAG_IMM8;
						}
						break;
					}
					if ( ProcessModRM == true )
					{						
						pDisasm->m_Flags |= FLAG_MODRM;
					}
				}
				break;
					
				case 0x80:
				case 0x81:
				case 0x82:
				case 0x83:
				case 0x84:
				case 0x85:
				case 0x86:
				case 0x87:
				case 0x88:
				case 0x89:
				case 0x8A:
				case 0x8B:
				case 0x8C:
				case 0x8D:
				case 0x8E:
				case 0x8F:
				{
					pDisasm->m_HasRelative = true;

					pDisasm->m_IsPrefixOperandSizeOverideValid = true;
				
					if ( pDisasm->m_PrefixOperandSizeOverride )
					{				
						pDisasm->m_Flags |= FLAG_REL16;
					}
					else
					{				
						pDisasm->m_Flags |= FLAG_REL32;
					}
				}
				break;
				
				case 0xA0:
				case 0xA1:
				case 0xA8:
				case 0xA9:
				{
					pDisasm->m_Flags |= FLAG_SEGMENT_REGISTER;
				}
				break;
				
				case 0xC8:
				case 0xC9:
				case 0xCA:
				case 0xCB:
				case 0xCC:
				case 0xCD:
				case 0xCE:
				case 0xCF:
				{
					pDisasm->m_Register = CurrentValue & 0x07;
				}
				break;
			}
		}
	}
	else
	{
		pDisasm->m_Opcode = CurrentValue;

		switch ( CurrentValue )
		{
			case 0x00:
			case 0x01:
			case 0x02:
			case 0x03:
			case 0x08:
			case 0x09:
			case 0x0A:
			case 0x0B:
			case 0x10:
			case 0x11:
			case 0x12:
			case 0x13:
			case 0x18:
			case 0x19:
			case 0x1A:
			case 0x1B:
			case 0x20:
			case 0x21:
			case 0x22:
			case 0x23:
			case 0x28:
			case 0x29:
			case 0x2A:
			case 0x2B:
			case 0x30:
			case 0x31:
			case 0x32:
			case 0x33:
			case 0x38:
			case 0x39:
			case 0x3A:
			case 0x3B:
			{				
				switch ( CurrentValue & 0x07 )
				{
					case 0x00:
					case 0x01:
					{
						pDisasm->m_IsLockPrefixValid = true;
					}
					break;
				}

				pDisasm->m_HasDirectionFlag = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				pDisasm->m_Flags |= FLAG_MODRM;
			}
			break;


			case 0x04:
			case 0x05:
			case 0x0C:
			case 0x0D:
			case 0x14:
			case 0x15:
			case 0x1C:
			case 0x1D:
			case 0x24:
			case 0x25:
			case 0x2C:
			case 0x2D:
			case 0x34:
			case 0x35:
			case 0x3C:
			case 0x3D:
			{
				pDisasm->m_HasImmediate = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				if ( ( CurrentValue & 0x07 ) == 5 )
				{
					if ( pDisasm->m_PrefixOperandSizeOverride )
					{
						pDisasm->m_Flags |= FLAG_IMM16;
					}
					else
					{
						pDisasm->m_Flags |= FLAG_IMM32;
					}
				}
				else
				{
					pDisasm->m_Flags |= FLAG_IMM8;
				}
			}
			break;

			case 0x06:
			case 0x07:
			case 0x0E:
			case 0x0F:
			case 0x16:
			case 0x17:
			case 0x1E:
			case 0x1F:
			{
				pDisasm->m_Flags |= FLAG_SEGMENT_REGISTER;
			}
			break;
			
			case 0x40:
			case 0x41:
			case 0x42:
			case 0x43:
			case 0x44:
			case 0x45:
			case 0x46:
			case 0x47:
			case 0x48:
			case 0x49:
			case 0x4A:
			case 0x4B:
			case 0x4C:
			case 0x4D:
			case 0x4E:
			case 0x4F:
			case 0x50:
			case 0x51:
			case 0x52:
			case 0x53:
			case 0x54:
			case 0x55:
			case 0x56:
			case 0x57:
			case 0x58:
			case 0x59:
			case 0x5A:
			case 0x5B:
			case 0x5C:
			case 0x5D:
			case 0x5E:
			case 0x5F:
			{
				pDisasm->m_Register = CurrentValue & 0x07;
			}
			break;

			case 0x60:
			case 0x61:
			{
				pDisasm->m_Register = ALL_GENERAL_PURPOSE;
			}
			break;

			case 0x62:
			case 0x63:
			{
				pDisasm->m_Flags |= FLAG_MODRM;
			}
			break;

			case 0x68:
			case 0x69:
			{
				pDisasm->m_HasImmediate = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				if ( pDisasm->m_PrefixOperandSizeOverride  )
				{
					pDisasm->m_Flags |= FLAG_IMM16;
				}
				else
				{
					pDisasm->m_Flags |= FLAG_IMM32;
				}

				if ( CurrentValue == 0x69 )
				{
					pDisasm->m_Flags |= FLAG_MODRM;
				}
			}
			break;

			case 0x6A:
			case 0x6B:
			{
				if ( CurrentValue == 0x6B )
				{
					pDisasm->m_HasImmediate = true;

					pDisasm->m_Flags |= FLAG_MODRM;
				}
				
				pDisasm->m_Flags |= FLAG_IMM8;
			}
			break;

			case 0x70:
			case 0x71:
			case 0x72:
			case 0x73:
			case 0x74:
			case 0x75:
			case 0x76:
			case 0x77:
			case 0x78:
			case 0x79:
			case 0x7A:
			case 0x7B:
			case 0x7C:
			case 0x7D:
			case 0x7E:
			case 0x7F:
			{
				pDisasm->m_HasRelative = true;
						
				pDisasm->m_Flags |= FLAG_REL8;
			}
			break;
			
			case 0x80:
			case 0x81:
			case 0x82:
			case 0x83:
			{
				pDisasm->m_HasImmediate = true;

				if ( CurrentValue != 0x81 )
				{					
					pDisasm->m_Flags |= FLAG_IMM8;
				}
				else
				{
					pDisasm->m_IsPrefixOperandSizeOverideValid = true;

					if ( pDisasm->m_PrefixOperandSizeOverride  )
					{				
						pDisasm->m_Flags |= FLAG_IMM16;
					}
					else
					{			
						pDisasm->m_Flags |= FLAG_IMM32;
					}
				}
								
				pDisasm->m_Flags |= FLAG_OPCEXT;
			}
			break;

			case 0x84:
			case 0x85:
			case 0x86:
			case 0x87:
			case 0x88:
			case 0x89:
			case 0x8A:
			case 0x8B:
			{
				pDisasm->m_HasDirectionFlag = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				pDisasm->m_Flags |= FLAG_MODRM;
			}
			break;

			case 0x8C:
			case 0x8D:
			case 0x8E:
			{
				if ( CurrentValue == 0x8C || CurrentValue == 0x8E )
				{
					pDisasm->m_HasDirectionFlag = true;
				}

				pDisasm->m_Flags |= FLAG_MODRM;
			}
			break;

			case 0x8F:
			{
				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				pDisasm->m_Flags |= FLAG_OPCEXT;
			}
			break;

			case 0x90:
			case 0x91:
			case 0x92:
			case 0x93:
			case 0x94:
			case 0x95:
			case 0x96:
			case 0x97:
			case 0x98:
			case 0x99:
			{
				if ( CurrentValue == 0x90 )
				{
					pDisasm->m_IsPrefixRepeatValid = true;
				}

				pDisasm->m_Register = CurrentValue & 0x07;
				pDisasm->m_IsRegisterInOpcode = true;
			}
			break;

			case 0x9A:
			{
				pDisasm->m_HasRelative = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;
				
				if ( pDisasm->m_PrefixOperandSizeOverride  )
				{				
					pDisasm->m_Flags |= FLAG_REL16;
				}
				else
				{				
					pDisasm->m_Flags |= FLAG_REL32;
				}

				pDisasm->m_Flags |= FLAG_SEGMENT_REL;
			}
			break;
			
			case 0xA0:
			case 0xA1:
			case 0xA2:
			case 0xA3:
			{
				pDisasm->m_HasDisplacement = true;

				pDisasm->m_IsPrefixAddressSizeOverrideValid = true;

				if ( pDisasm->m_REX_W )
				{
					if ( pDisasm->m_PrefixAddressSizeOverride )
					{
						pDisasm->Displacement.Displacement64 = *( unsigned long* )CurrentPointer;

						CurrentPointer += sizeof ( unsigned long );

						pDisasm->m_Flags |= FLAG_DISP32;
					}
					else
					{
						pDisasm->Displacement.Displacement64 = *( unsigned long long* )CurrentPointer;

						CurrentPointer += sizeof ( unsigned long long );

						pDisasm->m_Flags |= FLAG_DISP64;
					}
				}
				else
				{
					if ( pDisasm->m_PrefixAddressSizeOverride )
					{
						pDisasm->Displacement.Displacement64 = *( unsigned short* )CurrentPointer;

						CurrentPointer += sizeof ( unsigned short );

						pDisasm->m_Flags |= FLAG_DISP16;
					}
					else
					{
						pDisasm->Displacement.Displacement64 = *( unsigned long* )CurrentPointer;

						CurrentPointer += sizeof ( unsigned long );

						pDisasm->m_Flags |= FLAG_DISP32;
					}
				}
			}
			break;


			case 0xA8:
			{
				pDisasm->m_Flags |= FLAG_IMM8;
			}
			break;

			case 0xA9:
			{
				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				if ( pDisasm->m_PrefixOperandSizeOverride  )
				{				
					pDisasm->m_Flags |= FLAG_IMM16;
				}
				else
				{			
					pDisasm->m_Flags |= FLAG_IMM32;
				}
			}
			break;
			
			case 0xB0:
			case 0xB1:
			case 0xB2:
			case 0xB3:
			case 0xB4:
			case 0xB5:
			case 0xB6:
			case 0xB7:
			case 0xB8:
			case 0xB9:
			case 0xBA:
			case 0xBB:
			case 0xBC:
			case 0xBD:
			case 0xBE:
			case 0xBF:
			{
				pDisasm->m_HasImmediate = true;

				pDisasm->m_Register = CurrentValue & 0x07;
				pDisasm->m_IsRegisterInOpcode = true;

				if ( CurrentValue < 0xB8 )
				{
					pDisasm->m_Flags |= FLAG_IMM8;
				}
				else
				{
					pDisasm->m_IsPrefixOperandSizeOverideValid = true;
					pDisasm->m_IsPrefixAddressSizeOverrideValid = true;

					pDisasm->m_Register = CurrentValue & 0x07;

					if ( pDisasm->m_REX_W )
					{
						if ( pDisasm->m_PrefixAddressSizeOverride )
						{
							pDisasm->m_Flags |= FLAG_IMM32;
						}
						else
						{
							pDisasm->m_Flags |= FLAG_IMM64;
						}
					}
					else
					{
						if ( pDisasm->m_PrefixOperandSizeOverride )
						{
							pDisasm->m_Flags |= FLAG_IMM16;
						}
						else
						{
							pDisasm->m_Flags |= FLAG_IMM32;
						}
					}
				}
			}
			break;

			case 0xC0:
			case 0xC1:
			{
				pDisasm->m_HasImmediate = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				pDisasm->m_Flags |= FLAG_OPCEXT | FLAG_IMM8;
			}
			break;

			case 0xC2:
			{
				pDisasm->m_HasImmediate = true;

				pDisasm->m_Flags |= FLAG_IMM16;
			}
			break;
			
			case 0xC4:
			case 0xC5:
			{
				pDisasm->m_Flags |= FLAG_MODRM;
			}
			break;

			case 0xC6:
			case 0xC7:
			{					
				pDisasm->m_Flags |= FLAG_OPCEXT;
			}
			break;
			
			case 0xC8:
			case 0xCA:
			case 0xCD:
			{
				pDisasm->m_HasImmediate = true;

				switch ( CurrentValue )
				{
					case 0xC8:
					{
						pDisasm->m_Flags |= FLAG_IMM16 | FLAG_IMM8_2;
					}
					break;

					case 0xCA:
					{
						pDisasm->m_Flags |= FLAG_IMM16;
					}
					break;

					case 0xCD:
					{
						pDisasm->m_Flags |= FLAG_IMM8;
					}
					break;
				}
			}
			break;
			
			case 0xD0:
			case 0xD1:
			case 0xD2:
			case 0xD3:
			{
				pDisasm->m_IsPrefixOperandSizeOverideValid = true;
				
				pDisasm->m_Flags |= FLAG_OPCEXT;
			}
			break;
			
			case 0xD4:
			case 0xD5:
			{
				if ( *( CurrentPointer + 1 ) == 0x0A )
				{
					pDisasm->m_Opcode2 = *CurrentPointer++;
				}
				else
				{
					pDisasm->m_HasImmediate = true;

					pDisasm->m_Flags |= FLAG_IMM8;
				}
			}
			break;
			
			case 0xE0:
			case 0xE1:
			case 0xE2:
			case 0xE3:
			{
				pDisasm->m_HasRelative = true;

				pDisasm->m_Flags |= FLAG_REL8;
			}
			break;

			case 0xE4:
			case 0xE5:
			case 0xE6:
			case 0xE7:
			{
				pDisasm->m_HasImmediate = true;

				pDisasm->m_IsPrefixOperandSizeOverideValid = true;

				pDisasm->m_Flags |= FLAG_IMM8;
			}
			break;
			
			case 0xE8:
			case 0xE9:
			case 0xEA:
			case 0xEB:
			{
				pDisasm->m_HasRelative = true;

				switch ( CurrentValue )
				{
					case 0xE8:
					case 0xE9:
					case 0xEA:
					{
						pDisasm->m_IsPrefixOperandSizeOverideValid = true;

						if ( pDisasm->m_PrefixOperandSizeOverride )
						{				
							pDisasm->m_Flags |= FLAG_REL16;
						}
						else
						{				
							pDisasm->m_Flags |= FLAG_REL32;
						}

						if ( CurrentValue == 0xEA )
						{
							pDisasm->m_Flags |= FLAG_SEGMENT_REL;
						}
					}
					break;

					case 0xEB:
					{
						pDisasm->m_Flags |= FLAG_REL8;
					}
					break;
				}
			}
			break;

			case 0xD8:
			case 0xD9:
			case 0xDA:
			case 0xDB:
			case 0xDC:
			case 0xDD:
			case 0xDE:
			case 0xDF:
			case 0xF6:
			case 0xF7:
			case 0xFE:
			case 0xFF:
			{
				if ( CurrentValue == 0xF6 )
				{
					pDisasm->m_IsPrefixOperandSizeOverideValid = true;
				}
				
				pDisasm->m_Flags |= FLAG_OPCEXT;
			}
			break;
		}
	}

	if ( !( pDisasm->m_Flags & FLAG_3DNOW ) && pDisasm->m_Flags & FLAG_MODRM || pDisasm->m_Flags & FLAG_OPCEXT )
	{
		LDEModRMAndSIB3264 ( pDisasm, CurrentPointer );
	}
	if ( pDisasm->m_Flags & FLAG_PREFIX_0F )
	{
		switch ( CurrentValue )
		{	
			case 0xBA:
			{
				if ( pDisasm->m_ModRM_Reg > 4 )
				{
					pDisasm->m_IsLockPrefixValid = true;
				}

				if ( pDisasm->m_ModRM_Reg > 3 )
				{
					pDisasm->m_HasImmediate = true;

					pDisasm->m_Flags |= FLAG_IMM8;
				}
			}
			break;
		}
	}
	else
	{
		switch ( CurrentValue )
		{
			case 0x80:
			case 0x81:
			case 0x82:
			case 0x83:
			{
				if ( pDisasm->m_ModRM_Reg != 7 )
				{
					pDisasm->m_IsLockPrefixValid = true;
				}
			}
			break;

			case 0xC6:
			case 0xC7:
			{
				if ( pDisasm->m_ModRM_Reg == 0 )
				{
					pDisasm->m_HasImmediate = true;

					pDisasm->m_IsPrefixOperandSizeOverideValid = true;

					if ( CurrentValue == 0xC7 )
					{
						if ( pDisasm->m_PrefixOperandSizeOverride )
						{
							pDisasm->m_Flags |= FLAG_IMM16;
						}
						else
						{
							pDisasm->m_Flags |= FLAG_IMM32;
						}
					}
					else
					{
						pDisasm->m_Flags |= FLAG_IMM8;
					}
				}
			}
			break;

			case 0xF6:
			{
				if ( pDisasm->m_ModRM_Reg <= 1 )
				{
					pDisasm->m_HasImmediate = true;

					pDisasm->m_Flags |= FLAG_IMM8;
				}
			}
			break;

			case 0xF7:
			{
				if ( pDisasm->m_ModRM_Reg <= 4 )
				{
					pDisasm->m_IsPrefixOperandSizeOverideValid = true;
				}
				if ( pDisasm->m_ModRM_Reg <= 1 )
				{
					pDisasm->m_HasImmediate = true;

					if ( pDisasm->m_REX_W )
					{
						if ( pDisasm->m_PrefixAddressSizeOverride )
						{
							pDisasm->m_Flags |= FLAG_IMM32;
						}
						else
						{
							pDisasm->m_Flags |= FLAG_IMM64;
						}
					}
					else
					{
						if ( pDisasm->m_PrefixOperandSizeOverride )
						{
							pDisasm->m_Flags |= FLAG_IMM16;
						}
						else
						{
							pDisasm->m_Flags |= FLAG_IMM32;
						}
					}
				}
			}
			break;
		}
	}
}
//=============================================================================================================
void LDEModRMAndSIB3264 ( _Inout_ LDEDisasm_t* pDisasm, _Inout_ unsigned char*& CurrentPointer )
{
	pDisasm->m_Flags |= FLAG_MODRM;

	pDisasm->m_ModRM = *CurrentPointer++;

	LDEDecodeModRM3264 ( pDisasm );
	
	if ( pDisasm->m_UsingSIB )
	{
		pDisasm->m_SIB = *CurrentPointer++;

		LDEDecodeSIB3264 ( pDisasm );

		pDisasm->m_Flags |= FLAG_SIB;
	}

	if ( pDisasm->m_DisplacementSize != 0 )
	{
		if ( pDisasm->m_DisplacementSize == sizeof ( unsigned char ) )
		{
			pDisasm->Displacement.Displacement8 = *CurrentPointer++;

			pDisasm->m_HasDisplacement = true;

			pDisasm->m_Flags |= FLAG_DISP8;
		}
		else if ( pDisasm->m_DisplacementSize == sizeof ( unsigned short ) )
		{
			pDisasm->Displacement.Displacement16 = *( unsigned short* )CurrentPointer;

			pDisasm->m_HasDisplacement = true;

			CurrentPointer += sizeof ( unsigned short );

			pDisasm->m_Flags |= FLAG_DISP16;
		}
		else if ( pDisasm->m_DisplacementSize == sizeof ( unsigned long ) )
		{
			pDisasm->Displacement.Displacement32 = *( unsigned long* )CurrentPointer;

			pDisasm->m_HasDisplacement = true;

			CurrentPointer += sizeof ( unsigned long );

			pDisasm->m_Flags |= FLAG_DISP32;
		}
	}
}
//=============================================================================================================
void LDEDecodeModRM3264 ( _Inout_ LDEDisasm_t* pDisasm )
{
	pDisasm->m_ModRM_Mod = pDisasm->m_ModRM >> 6;
	pDisasm->m_ModRM_Reg = ( pDisasm->m_ModRM & 0x3F ) >> 3;
	pDisasm->m_ModRM_RM = pDisasm->m_ModRM & 7;

	pDisasm->m_DisplacementSize = 0;
	pDisasm->m_UseRegisterToRegister = false;
	pDisasm->m_UsingSIB = false;

	switch ( pDisasm->m_ModRM_Mod )
	{
		case 0:
		{
			// register indirect addressing mode
			// or sib with no displacement when R/M is 100

			if ( pDisasm->m_PrefixAddressSizeOverride )
			{
				if ( pDisasm->m_ModRM_RM == 6 )
				{
					pDisasm->m_DisplacementSize = sizeof ( unsigned short );
				}
			}
			else
			{
				if ( pDisasm->m_ModRM_RM == 5 )
				{
					pDisasm->m_DisplacementSize = sizeof ( unsigned long );
				}
			}
		}
		break;

		case 1:
		{
			pDisasm->m_DisplacementSize = sizeof ( unsigned char );
		}
		break;
		
		case 2:
		{
			pDisasm->m_DisplacementSize = sizeof ( unsigned short );

			if ( !pDisasm->m_Prefix67 )
			{
				pDisasm->m_DisplacementSize <<= 1;
			}
		}
		break;

		case 3:
		{				
			pDisasm->m_UseRegisterToRegister = true; // register to register
		}
		break;
	}

	if ( pDisasm->m_ModRM_RM == 4 ) // sib byte follows, unless modrm_mod is 3, in which case we use rsp/esp
	{
		if ( pDisasm->m_UseRegisterToRegister && !pDisasm->m_PrefixAddressSizeOverride )
		{
			pDisasm->m_RMRegister = REG_ESP; // rsp

			pDisasm->m_UsingSIB = false;
		}
		else
		{
			pDisasm->m_UsingSIB = true;
		}
	}
	if ( pDisasm->m_ModRM_RM == 5 ) // displacement addressing
	{
		if ( pDisasm->m_ModRM_Mod != 0 )
		{
			pDisasm->m_RMRegister = REG_EBP; // rbp
		}
		else
		{
			pDisasm->m_UseDisplacementOnly = true;
		}
	}
}
//=============================================================================================================
void LDEDecodeSIB3264 ( _Inout_ LDEDisasm_t* pDisasm )
{
	pDisasm->m_SIB_Scale = pDisasm->m_SIB >> 6;
	pDisasm->m_SIB_Index = ( pDisasm->m_SIB & 0x3F ) >> 3;
	pDisasm->m_SIB_Base = pDisasm->m_SIB & 7;
		
	pDisasm->m_Scalar = ( 1 << pDisasm->m_SIB_Scale );

	if ( pDisasm->m_SIB_Index == 4 ) // illegal, set invalid register
	{
		pDisasm->m_SIB_Index = REG_INVALID;
	}
	if ( pDisasm->m_SIB_Base == 5 )
	{
		if ( pDisasm->m_ModRM_Mod != 1 )
		{
			pDisasm->m_DisplacementSize = sizeof ( unsigned long );
		}
		else
		{
			pDisasm->m_DisplacementSize = sizeof ( unsigned char );
		}
	}
}
//=============================================================================================================