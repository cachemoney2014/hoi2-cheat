//=============================================================================================================
#include "Required.h"
//=============================================================================================================
#include "Cvars.h"
#include "Defines.h"
#include "DHExecutable.h"
#include "Globals.h"
//=============================================================================================================
std::map< unsigned int, unsigned int > g_MapOffsets;
//=============================================================================================================
unsigned int HashString ( const char *String )
{
	unsigned int Hash = 32;

	int Length = ( int )strlen ( String );

	for ( int i = 0; i < Length; i++ )
	{
		Hash += Hash ^ String[ i ];
	}

	return Hash;
}
//=============================================================================================================
void InitializeResourceTab ( HINSTANCE hInstance )
{
	HWND Box;

	int Vertical;

	RECT tr;

	tr = {0};

	TabCtrl_GetItemRect ( g_TabControl, 0, &tr );

	Vertical = tr.bottom - tr.top;

	ChildWindow_t ChildWindow;

	memset ( &ChildWindow, 0, sizeof ( ChildWindow_t ) );
	
	for ( size_t Iterator = 0; Iterator < RESOURCE_ARRAY_SIZE; Iterator++ )
	{						
		CreateWindowExA ( 0, "Static", g_ResourceNames[ Iterator ], WS_DLGFRAME | WS_CHILD | WS_VISIBLE, 0, Vertical, 100, 30, g_Tabs[ TAB_RESOURCES ].m_Window, NULL, hInstance, NULL );
		Box = CreateWindowExA ( 0, "Edit", "", WS_DLGFRAME | WS_CHILD | WS_VISIBLE, 101, Vertical, 100, 30, g_Tabs[ TAB_RESOURCES ].m_Window, ( HMENU )Iterator + EDITBOX_RESOURCE_OIL, hInstance, NULL );
		
		ChildWindow.m_CVar = EditBoxResources[ Iterator ];
		ChildWindow.m_HMENU = Iterator + EDITBOX_RESOURCE_OIL;
		ChildWindow.m_Type = EDITBOX;
		ChildWindow.m_Window = Box;
		
		g_Tabs[ TAB_RESOURCES ].m_ChildWindows.push_back ( ChildWindow );
		
		Box = CreateWindowExA ( 0, "Button", "Freeze", WS_DLGFRAME | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, 201, Vertical, 75, 30, g_Tabs[ TAB_RESOURCES ].m_Window, ( HMENU )Iterator + CHECKBOX_FREEZE_RESOURCE_OIL, hInstance, NULL );

		ChildWindow.m_CVar = CheckBoxFreezeResources[ Iterator ];
		ChildWindow.m_HMENU = Iterator + CHECKBOX_FREEZE_RESOURCE_OIL;
		ChildWindow.m_Type = CHECKBOX;
		ChildWindow.m_SubType |= CHECKBOX_FREEZE;
		ChildWindow.m_Window = Box;
		
		g_Tabs[ TAB_RESOURCES ].m_ChildWindows.push_back ( ChildWindow );

		Box = CreateWindowExA ( 0, "Button", "Max", WS_DLGFRAME | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, 277, Vertical, 60, 30, g_Tabs[ TAB_RESOURCES ].m_Window, ( HMENU )Iterator + CHECKBOX_MAX_RESOURCE_OIL, hInstance, NULL );
		
		ChildWindow.m_CVar = CheckBoxMaxResources[ Iterator ];
		ChildWindow.m_HMENU = Iterator + CHECKBOX_MAX_RESOURCE_OIL;
		ChildWindow.m_Type = CHECKBOX;
		ChildWindow.m_SubType |= CHECKBOX_MAX;
		ChildWindow.m_Window = Box;
				
		g_Tabs[ TAB_RESOURCES ].m_ChildWindows.push_back ( ChildWindow );
		
		Vertical += 30;
	}
}
//=============================================================================================================
void InitializeTechnologyTab ( HINSTANCE hInstance )
{

}
//=============================================================================================================
void InitializeProvinceTab ( HINSTANCE hInstance )
{

}
//=============================================================================================================
void InitializeUnitModifiersTab ( HINSTANCE hInstance )
{

}
//=============================================================================================================
void InitializeGlobalVariablesTab ( HINSTANCE hInstance )
{
	HWND Box;

	int Horizontal, Vertical;

	RECT MainWindow;

	LONG x, y;

	RECT tr;

	tr = {0};

	TabCtrl_GetItemRect ( g_TabControl, 0, &tr );

	Vertical = tr.bottom - tr.top;

	ChildWindow_t ChildWindow;

	GetWindowRect ( g_MainWindow, &MainWindow );

	Horizontal = 0;

	memset ( &ChildWindow, 0, sizeof ( ChildWindow_t ) );
	
	for ( size_t Iterator = 0; Iterator < MISSION_ARRAY_SIZE; Iterator++ )
	{
		CreateWindowExA ( 0, "Static", g_MissionNames[ Iterator ], WS_DLGFRAME | WS_CHILD | WS_VISIBLE, Horizontal, Vertical, 100, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, NULL, hInstance, NULL );
		Box = CreateWindowExA ( 0, "Edit", "", WS_DLGFRAME | WS_CHILD | WS_VISIBLE, Horizontal + 101, Vertical, 100, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, ( HMENU )Iterator + EDITBOX_MISSIONS_MISSION_ATTACK, hInstance, NULL );
		
		ChildWindow.m_CVar = EditBoxMissions[ Iterator ];
		ChildWindow.m_HMENU = Iterator + EDITBOX_MISSIONS_MISSION_ATTACK;
		ChildWindow.m_Type = EDITBOX;
		ChildWindow.m_Window = Box;
		
		g_Tabs[ TAB_GLOBALVARIABLES ].m_ChildWindows.push_back ( ChildWindow );

		Box = CreateWindowExA ( 0, "Button", "Enabled", WS_DLGFRAME | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, Horizontal + 201, Vertical, 80, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, ( HMENU )Iterator + CHECKBOX_MISSIONS_MISSION_ATTACK, hInstance, NULL );
		
		ChildWindow.m_CVar = CheckBoxEnableMissions[ Iterator ];
		ChildWindow.m_HMENU = Iterator + CHECKBOX_MISSIONS_MISSION_ATTACK;
		ChildWindow.m_Type = CHECKBOX;
		ChildWindow.m_SubType |= CHECKBOX_ENABLED;
		ChildWindow.m_Window = Box;

		g_Tabs[ TAB_GLOBALVARIABLES ].m_ChildWindows.push_back ( ChildWindow );

		Vertical += 40;

		if ( Vertical > ( tr.bottom - tr.top ) + g_Tabs[ TAB_GLOBALVARIABLES ].m_Height - 160 )
		{
			Vertical = tr.bottom - tr.top;
			
			Horizontal += 281;
		}
	}

	memset ( &ChildWindow, 0, sizeof ( ChildWindow_t ) );

	for ( size_t Iterator = 0; Iterator < PRODUCTION_AND_SUPPLIES_ARRAY_SIZE; Iterator++ )
	{
		CreateWindowExA ( 0, "Static", g_ProductionAndSupplies[ Iterator ], WS_DLGFRAME | WS_CHILD | WS_VISIBLE, Horizontal, Vertical, 100, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, NULL, hInstance, NULL );
		Box = CreateWindowExA ( 0, "Edit", "", WS_DLGFRAME | WS_CHILD | WS_VISIBLE, Horizontal + 101, Vertical, 100, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, ( HMENU )Iterator + EDITBOX_PRODUCTION_AND_SUPPLIES_TC_MODIFIER, hInstance, NULL );
		
		ChildWindow.m_CVar = EditBoxProductionAndSupplies[ Iterator ];
		ChildWindow.m_HMENU = Iterator + EDITBOX_PRODUCTION_AND_SUPPLIES_TC_MODIFIER;
		ChildWindow.m_Type = EDITBOX;
		ChildWindow.m_Window = Box;
		
		g_Tabs[ TAB_GLOBALVARIABLES ].m_ChildWindows.push_back ( ChildWindow );
				
		Box = CreateWindowExA ( 0, "Button", "Max", WS_DLGFRAME | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, Horizontal + 201, Vertical, 80, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, ( HMENU )Iterator + CHECKBOX_PRODUCTION_AND_SUPPLIES_TC_MODIFIER, hInstance, NULL );
		
		ChildWindow.m_CVar = CheckBoxMaxProductionAndSupplies[ Iterator ];
		ChildWindow.m_HMENU = Iterator + CHECKBOX_PRODUCTION_AND_SUPPLIES_TC_MODIFIER;
		ChildWindow.m_Type = CHECKBOX;
		ChildWindow.m_SubType |= CHECKBOX_MAX;
		ChildWindow.m_Window = Box;
				
		g_Tabs[ TAB_GLOBALVARIABLES ].m_ChildWindows.push_back ( ChildWindow );
		
		Vertical += 40;

		if ( Vertical > ( tr.bottom - tr.top ) + g_Tabs[ TAB_GLOBALVARIABLES ].m_Height - 160 )
		{
			Vertical = tr.bottom - tr.top;
			
			Horizontal += 351;
		}
	}

	memset ( &ChildWindow, 0, sizeof ( ChildWindow_t ) );

	for ( size_t Iterator = 0; Iterator < COMBAT_EVENTS_ARRAY_SIZE; Iterator++ )
	{
		CreateWindowExA ( 0, "Static", g_CombatEvents[ Iterator ], WS_DLGFRAME | WS_CHILD | WS_VISIBLE, Horizontal, Vertical, 100, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, NULL, hInstance, NULL );
		Box = CreateWindowExA ( 0, "Edit", "", WS_DLGFRAME | WS_CHILD | WS_VISIBLE, Horizontal + 101, Vertical, 100, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, ( HMENU )Iterator + EDITBOX_COMBAT_EVENTS_COUNTERATTACK, hInstance, NULL );
		
		ChildWindow.m_CVar = EditBoxCombatEvents[ Iterator ];
		ChildWindow.m_HMENU = Iterator + EDITBOX_COMBAT_EVENTS_COUNTERATTACK;
		ChildWindow.m_Type = EDITBOX;
		ChildWindow.m_Window = Box;
		
		g_Tabs[ TAB_GLOBALVARIABLES ].m_ChildWindows.push_back ( ChildWindow );
				
		Box = CreateWindowExA ( 0, "Button", "Max", WS_DLGFRAME | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, Horizontal + 201, Vertical, 80, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, ( HMENU )Iterator + CHECKBOX_COMBAT_EVENTS_COUNTERATTACK, hInstance, NULL );
		
		ChildWindow.m_CVar = CheckBoxMaxCombatEvents[ Iterator ];
		ChildWindow.m_HMENU = Iterator + CHECKBOX_COMBAT_EVENTS_COUNTERATTACK;
		ChildWindow.m_Type = CHECKBOX;
		ChildWindow.m_SubType |= CHECKBOX_MAX;
		ChildWindow.m_Window = Box;
				
		g_Tabs[ TAB_GLOBALVARIABLES ].m_ChildWindows.push_back ( ChildWindow );
		
		Vertical += 40;

		if ( Vertical > ( tr.bottom - tr.top ) + g_Tabs[ TAB_GLOBALVARIABLES ].m_Height - 160 )
		{
			Vertical = tr.bottom - tr.top;
			
			Horizontal += 281;
		}
	}

	memset ( &ChildWindow, 0, sizeof ( ChildWindow_t ) );

	for ( size_t Iterator = 0; Iterator < MISC_MILITARY_ARRAY_SIZE; Iterator++ )
	{
		CreateWindowExA ( 0, "Static", g_MiscMilitary[ Iterator ], WS_DLGFRAME | WS_CHILD | WS_VISIBLE, Horizontal, Vertical, 100, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, NULL, hInstance, NULL );
		Box = CreateWindowExA ( 0, "Edit", "", WS_DLGFRAME | WS_CHILD | WS_VISIBLE, Horizontal + 101, Vertical, 100, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, ( HMENU )Iterator + EDITBOX_MISC_MILITARY_ARMIES_CAN_DIG_IN, hInstance, NULL );
		
		ChildWindow.m_CVar = EditBoxMiscMilitary[ Iterator ];
		ChildWindow.m_HMENU = Iterator + EDITBOX_MISC_MILITARY_ARMIES_CAN_DIG_IN;
		ChildWindow.m_Type = EDITBOX;
		ChildWindow.m_Window = Box;
		
		g_Tabs[ TAB_GLOBALVARIABLES ].m_ChildWindows.push_back ( ChildWindow );
				
		Box = CreateWindowExA ( 0, "Button", "Max", WS_DLGFRAME | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, Horizontal + 201, Vertical, 80, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, ( HMENU )Iterator + CHECKBOX_MISC_MILITARY_ARMIES_CAN_DIG_IN, hInstance, NULL );
		
		ChildWindow.m_CVar = CheckBoxMaxMiscMilitary[ Iterator ];
		ChildWindow.m_HMENU = Iterator + CHECKBOX_MISC_MILITARY_ARMIES_CAN_DIG_IN;
		ChildWindow.m_Type = CHECKBOX;
		ChildWindow.m_SubType |= CHECKBOX_MAX;
		ChildWindow.m_Window = Box;
				
		g_Tabs[ TAB_GLOBALVARIABLES ].m_ChildWindows.push_back ( ChildWindow );
		
		Vertical += 40;

		if ( Vertical > ( tr.bottom - tr.top ) + g_Tabs[ TAB_GLOBALVARIABLES ].m_Height - 160 )
		{
			Vertical = tr.bottom - tr.top;
			
			Horizontal += 281;
		}
	}

	memset ( &ChildWindow, 0, sizeof ( ChildWindow_t ) );

	for ( size_t Iterator = 0; Iterator < INTEL_AND_RESEARCH_ARRAY_SIZE; Iterator++ )
	{
		CreateWindowExA ( 0, "Static", g_IntelAndResearch[ Iterator ], WS_DLGFRAME | WS_CHILD | WS_VISIBLE, Horizontal, Vertical, 100, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, NULL, hInstance, NULL );
		Box = CreateWindowExA ( 0, "Edit", "", WS_DLGFRAME | WS_CHILD | WS_VISIBLE, Horizontal + 101, Vertical, 100, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, ( HMENU )Iterator + EDITBOX_INTEL_AND_RESEARCH_FRIENDLY_INTEL_CHANCE, hInstance, NULL );
		
		ChildWindow.m_CVar = EditBoxIntelAndResearch[ Iterator ];
		ChildWindow.m_HMENU = Iterator + EDITBOX_INTEL_AND_RESEARCH_FRIENDLY_INTEL_CHANCE;
		ChildWindow.m_Type = EDITBOX;
		ChildWindow.m_Window = Box;
		
		g_Tabs[ TAB_GLOBALVARIABLES ].m_ChildWindows.push_back ( ChildWindow );
				
		Box = CreateWindowExA ( 0, "Button", "Max", WS_DLGFRAME | WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, Horizontal + 201, Vertical, 80, 40, g_Tabs[ TAB_GLOBALVARIABLES ].m_Window, ( HMENU )Iterator + CHECKBOX_INTEL_AND_RESEARCH_FRIENDLY_INTEL_CHANCE, hInstance, NULL );
		
		ChildWindow.m_CVar = CheckBoxMaxIntelAndResearch[ Iterator ];
		ChildWindow.m_HMENU = Iterator + CHECKBOX_INTEL_AND_RESEARCH_FRIENDLY_INTEL_CHANCE;
		ChildWindow.m_Type = CHECKBOX;
		ChildWindow.m_SubType |= CHECKBOX_MAX;
		ChildWindow.m_Window = Box;
				
		g_Tabs[ TAB_GLOBALVARIABLES ].m_ChildWindows.push_back ( ChildWindow );
		
		Vertical += 40;

		if ( Vertical > ( tr.bottom - tr.top ) + g_Tabs[ TAB_GLOBALVARIABLES ].m_Height - 160 )
		{
			Vertical = tr.bottom - tr.top;
			
			Horizontal += 281;
		}
	}
}
//=============================================================================================================
void InitializeDiplomacyTab ( HINSTANCE hInstance )
{

}
//=============================================================================================================
void InitializeOffsets ( void )
{
	for ( uintptr_t Iterator = 0; Iterator < RESOURCE_ARRAY_SIZE; Iterator++ )
	{
		g_MapOffsets[ HashString ( CheckBoxFreezeResources[ Iterator ]->GetIniString() ) ] = DH_OilOffset_Displacement + Iterator * sizeof ( uintptr_t );
		g_MapOffsets[ HashString ( CheckBoxMaxResources[ Iterator ]->GetIniString() ) ] = DH_OilOffset_Displacement + Iterator * sizeof ( uintptr_t );
		g_MapOffsets[ HashString ( EditBoxResources[ Iterator ]->GetIniString() ) ] = DH_OilOffset_Displacement + Iterator * sizeof ( uintptr_t );
	}

	for ( uintptr_t Iterator = 0; Iterator < RESOURCE_ARRAY_SIZE; Iterator++ )
	{
		CheckBoxMaxResources[ Iterator ]->SetParent ( EditBoxResources[ Iterator ] );
	}

	g_MapOffsets[ HashString ( CheckBoxFreezeResources[ 7 ]->GetIniString() ) ] = DH_NukeOffset_Displacement;
	g_MapOffsets[ HashString ( CheckBoxMaxResources[ 7 ]->GetIniString() ) ] = DH_NukeOffset_Displacement;
	g_MapOffsets[ HashString ( EditBoxResources[ 7 ]->GetIniString() ) ] = DH_NukeOffset_Displacement;

	for ( uintptr_t Iterator = 0; Iterator < MISSION_ARRAY_SIZE; Iterator++ )
	{
//		g_MapOffsets[ HashString ( CheckBoxMaxMissions[ Iterator ]->GetIniString() ) ] = DH_MissionOffset_Start_Immediate + Iterator * sizeof ( uintptr_t );
		g_MapOffsets[ HashString ( EditBoxMissions[ Iterator ]->GetIniString() ) ] = DH_MissionOffset_Start_Immediate + Iterator * sizeof ( uintptr_t );
	}

	for ( uintptr_t Iterator = 0; Iterator < MISSION_ARRAY_SIZE; Iterator++ )
	{
		g_MapOffsets[ HashString ( CheckBoxEnableMissions[ Iterator ]->GetIniString() ) ] = DH_IsMissionEnabledOffset_Displacement + Iterator + 1;
	}

	g_MapOffsets[ HashString ( EditBoxMissions[ 33 ]->GetIniString() ) ] = DH_AmphibiousSize_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxProductionAndSupplies[ 0 ]->GetIniString() ) ] = DH_TransportCapacityModifer_Displacement;
	g_MapOffsets[ HashString ( EditBoxProductionAndSupplies[ 0 ]->GetIniString() ) ] = DH_TransportCapacityModifer_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxProductionAndSupplies[ 1 ]->GetIniString() ) ] = DH_TransportCapacityOccupiedModifer_Displacement;
	g_MapOffsets[ HashString ( EditBoxProductionAndSupplies[ 1 ]->GetIniString() ) ] = DH_TransportCapacityOccupiedModifer_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxProductionAndSupplies[ 2 ]->GetIniString() ) ] = DH_SupplyToDistanceModifier_Displacement;
	g_MapOffsets[ HashString ( EditBoxProductionAndSupplies[ 2 ]->GetIniString() ) ] = DH_SupplyToDistanceModifier_Displacement;
	
	g_MapOffsets[ HashString ( CheckBoxMaxProductionAndSupplies[ 3 ]->GetIniString() ) ] = DH_RepairModifier_Displacement;
	g_MapOffsets[ HashString ( EditBoxProductionAndSupplies[ 3 ]->GetIniString() ) ] = DH_RepairModifier_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxProductionAndSupplies[ 4 ]->GetIniString() ) ] = DH_EnergyToOil_Displacement;
	g_MapOffsets[ HashString ( EditBoxProductionAndSupplies[ 4 ]->GetIniString() ) ] = DH_EnergyToOil_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxProductionAndSupplies[ 5 ]->GetIniString() ) ] = DH_ProductionEfficiency_Displacement;
	g_MapOffsets[ HashString ( EditBoxProductionAndSupplies[ 5 ]->GetIniString() ) ] = DH_ProductionEfficiency_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxProductionAndSupplies[ 6 ]->GetIniString() ) ] = DH_SupplyProductionEfficiency_Displacement;
	g_MapOffsets[ HashString ( EditBoxProductionAndSupplies[ 6 ]->GetIniString() ) ] = DH_SupplyProductionEfficiency_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxProductionAndSupplies[ 7 ]->GetIniString() ) ] = DH_ManpowerGrowth_Displacement;
	g_MapOffsets[ HashString ( EditBoxProductionAndSupplies[ 7 ]->GetIniString() ) ] = DH_ManpowerGrowth_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxProductionAndSupplies[ 8 ]->GetIniString() ) ] = DH_Trickleback_Displacement;
	g_MapOffsets[ HashString ( EditBoxProductionAndSupplies[ 8 ]->GetIniString() ) ] = DH_Trickleback_Displacement;

	for ( uintptr_t Iterator = 0; Iterator < PRODUCTION_AND_SUPPLIES_ARRAY_SIZE; Iterator++ )
	{
		CheckBoxMaxProductionAndSupplies[ Iterator ]->SetParent ( EditBoxProductionAndSupplies[ Iterator ] );
	}

	for ( uintptr_t Iterator = 0; Iterator < COMBAT_EVENTS_ARRAY_SIZE; Iterator++ )
	{
		g_MapOffsets[ HashString ( CheckBoxMaxCombatEvents[ Iterator ]->GetIniString() ) ] = DH_CombatEvents_Displacement + Iterator * sizeof ( uintptr_t );
		g_MapOffsets[ HashString ( EditBoxCombatEvents[ Iterator ]->GetIniString() ) ] = DH_CombatEvents_Displacement + Iterator * sizeof ( uintptr_t );
	}

	for ( uintptr_t Iterator = 0; Iterator < COMBAT_EVENTS_ARRAY_SIZE; Iterator++ )
	{
		CheckBoxMaxCombatEvents[ Iterator ]->SetParent ( EditBoxCombatEvents[ Iterator ] );
	}

	g_MapOffsets[ HashString ( CheckBoxMaxCombatEvents[ 7 ]->GetIniString() ) ] = DH_CombatEvents_HQ_Bonus_Chance_Displacement;
	g_MapOffsets[ HashString ( EditBoxCombatEvents[ 7 ]->GetIniString() ) ] = DH_CombatEvents_HQ_Bonus_Chance_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxMiscMilitary[ 0 ]->GetIniString() ) ] = DH_MiscMilitary_DigIn_Displacement;
	g_MapOffsets[ HashString ( EditBoxMiscMilitary[ 0 ]->GetIniString() ) ] = DH_MiscMilitary_DigIn_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxMiscMilitary[ 1 ]->GetIniString() ) ] = DH_MiscMilitary_Nuke_Displacement;
	g_MapOffsets[ HashString ( EditBoxMiscMilitary[ 1 ]->GetIniString() ) ] = DH_MiscMilitary_Nuke_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxMiscMilitary[ 2 ]->GetIniString() ) ] = DH_MiscMilitary_AntiAirEfficiency_Displacement;
	g_MapOffsets[ HashString ( EditBoxMiscMilitary[ 2 ]->GetIniString() ) ] = DH_MiscMilitary_AntiAirEfficiency_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxMiscMilitary[ 3 ]->GetIniString() ) ] = DH_MiscMilitary_Attrition_Displacement;
	g_MapOffsets[ HashString ( EditBoxMiscMilitary[ 3 ]->GetIniString() ) ] = DH_MiscMilitary_Attrition_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxMiscMilitary[ 4 ]->GetIniString() ) ] = DH_MiscMilitary_HQEffectiveSupplyEfficiency_Displacement;
	g_MapOffsets[ HashString ( EditBoxMiscMilitary[ 4 ]->GetIniString() ) ] = DH_MiscMilitary_HQEffectiveSupplyEfficiency_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxMiscMilitary[ 5 ]->GetIniString() ) ] = DH_MiscMilitary_GroundDefenseEfficiency_Displacement;
	g_MapOffsets[ HashString ( EditBoxMiscMilitary[ 5 ]->GetIniString() ) ] = DH_MiscMilitary_GroundDefenseEfficiency_Displacement;
	
	g_MapOffsets[ HashString ( CheckBoxMaxMiscMilitary[ 6 ]->GetIniString() ) ] = DH_MiscMilitary_ConvoyDefenseEfficiency_Displacement;
	g_MapOffsets[ HashString ( EditBoxMiscMilitary[ 6 ]->GetIniString() ) ] = DH_MiscMilitary_ConvoyDefenseEfficiency_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxMiscMilitary[ 7 ]->GetIniString() ) ] = DH_MiscMilitary_LandFortEfficiency_Displacement;
	g_MapOffsets[ HashString ( EditBoxMiscMilitary[ 7 ]->GetIniString() ) ] = DH_MiscMilitary_LandFortEfficiency_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxMiscMilitary[ 8 ]->GetIniString() ) ] = DH_MiscMilitary_CoastFortEfficiency_Displacement;
	g_MapOffsets[ HashString ( EditBoxMiscMilitary[ 8 ]->GetIniString() ) ] = DH_MiscMilitary_CoastFortEfficiency_Displacement;
	
	g_MapOffsets[ HashString ( CheckBoxMaxIntelAndResearch[ 0 ]->GetIniString() ) ] = DH_Intel_and_Research_FriendlyIntelChance_Displacement;
	g_MapOffsets[ HashString ( EditBoxIntelAndResearch[ 0 ]->GetIniString() ) ] = DH_Intel_and_Research_FriendlyIntelChance_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxIntelAndResearch[ 1 ]->GetIniString() ) ] = DH_Intel_and_Research_EnemyIntelChance_Displacement;
	g_MapOffsets[ HashString ( EditBoxIntelAndResearch[ 1 ]->GetIniString() ) ] = DH_Intel_and_Research_EnemyIntelChance_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxIntelAndResearch[ 2 ]->GetIniString() ) ] = DH_Intel_and_Research_RadarEfficiency_Displacement;
	g_MapOffsets[ HashString ( EditBoxIntelAndResearch[ 2 ]->GetIniString() ) ] = DH_Intel_and_Research_RadarEfficiency_Displacement;

	g_MapOffsets[ HashString ( CheckBoxMaxIntelAndResearch[ 3 ]->GetIniString() ) ] = DH_Intel_and_Research_ResearchSpeed_Displacement;
	g_MapOffsets[ HashString ( EditBoxIntelAndResearch[ 3 ]->GetIniString() ) ] = DH_Intel_and_Research_ResearchSpeed_Displacement;

	for ( uintptr_t Iterator = 0; Iterator < MISC_MILITARY_ARRAY_SIZE; Iterator++ )
	{
		CheckBoxMaxMiscMilitary[ Iterator ]->SetParent ( EditBoxMiscMilitary[ Iterator ] );
	}
	for ( uintptr_t Iterator = 0; Iterator < INTEL_AND_RESEARCH_ARRAY_SIZE; Iterator++ )
	{
		CheckBoxMaxIntelAndResearch[ Iterator ]->SetParent ( EditBoxIntelAndResearch[ Iterator ] );
	}
}
//=============================================================================================================