//=============================================================================================================
#include "Required.h"
//=============================================================================================================
#include "Cvars.h"
#include "Defines.h"
#include "Globals.h"
#include "Hack.h"
#include "Init.h"
#include "LDE.h"
#include "Logic.h"
#include "Memory.h"
//=============================================================================================================
HANDLE g_ProcessGame = INVALID_HANDLE_VALUE;
uintptr_t g_ProcessID = 0;
uintptr_t g_GameProcessBase = 0;
uintptr_t g_GameProcessSize = 0;
uintptr_t g_GameProcessCodeSectionSize = 0;
//=============================================================================================================
char* GameProcess[] =
{
	AOD_NAME,
	DH_NAME,
	HOI_NAME
};
//========================================================================================
BOOL EnableTokenPrivilege ( LPCTSTR Privilege )
{
	HANDLE Token		 = 0;
	TOKEN_PRIVILEGES tkp = {0}; 

	if ( !OpenProcessToken ( GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &Token ) )
	{
		return FALSE;
	}
	
	if ( LookupPrivilegeValue ( NULL, Privilege, &tkp.Privileges[0].Luid ) ) 
	{
        tkp.PrivilegeCount = 1;  
		tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

		if ( AdjustTokenPrivileges ( Token, FALSE, &tkp, 0, ( PTOKEN_PRIVILEGES )NULL, 0 ) )
		{
			return TRUE;
		}
	}
	
	return FALSE;
}
//=============================================================================================================
bool GetGameProcess ( void )
{
	MEMORY_BASIC_INFORMATION MBI;

	if ( !EnableTokenPrivilege ( SE_DEBUG_NAME ) )
	{
		return false;
	}

	if ( g_ProcessGame != INVALID_HANDLE_VALUE )
	{
		if ( !VirtualQueryEx ( g_ProcessGame, ( LPCVOID )g_GameProcessBase, &MBI, sizeof ( MEMORY_BASIC_INFORMATION ) ) )
		{
			return false;
		}

		return true;
	}
		
	HANDLE ProcessSnap;

	PROCESSENTRY32 pe32;

	ProcessSnap = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );

	if ( ProcessSnap == INVALID_HANDLE_VALUE )
	{
		return false;
	}
	
	pe32.dwSize = sizeof ( PROCESSENTRY32 );
	
	if ( !Process32First( ProcessSnap, &pe32 ) )
	{
		CloseHandle ( ProcessSnap );

		return false;
	}
	do
	{
		for ( size_t Iterator = 0; Iterator < 3; Iterator++ )
		{
			if ( !strcmp ( pe32.szExeFile, GameProcess[ Iterator ] ) )
			{
				g_ProcessGame = OpenProcess ( PROCESS_TERMINATE | PROCESS_VM_OPERATION | PROCESS_VM_READ |
					PROCESS_VM_WRITE | PROCESS_SET_INFORMATION | PROCESS_QUERY_INFORMATION | PROCESS_SUSPEND_RESUME, FALSE, pe32.th32ProcessID );

				if ( g_ProcessGame == INVALID_HANDLE_VALUE )
				{
					return false;
				}
				else 
				{	
					g_ProcessID = pe32.th32ProcessID;

					g_Memory.Set ( g_ProcessGame );

					return true;
				}
			}
		}
		
	}while ( Process32Next ( ProcessSnap, &pe32 ) );

	CloseHandle ( ProcessSnap );

	return false;
}
//=============================================================================================================
bool GetGameModule ( void )
{
	HANDLE ModuleSnap = INVALID_HANDLE_VALUE;
	MODULEENTRY32 me32;

	if ( g_GameProcessBase != 0 && g_GameProcessSize != 0 )
	{
		return true;
	}

	ModuleSnap = CreateToolhelp32Snapshot ( TH32CS_SNAPMODULE, g_ProcessID );

	if ( ModuleSnap == INVALID_HANDLE_VALUE )
	{
		CloseHandle ( ModuleSnap ); 

		return false;
	}

	me32.dwSize = sizeof ( MODULEENTRY32 );
	
	if ( !Module32First ( ModuleSnap, &me32 ) )
	{
		CloseHandle ( ModuleSnap );

		return false;
	}
	do
	{
		for ( size_t Iterator = 0; Iterator < 3; Iterator++ )
		{
			if ( strstr ( me32.szModule, GameProcess[ Iterator ] ) )
			{
				g_GameProcessBase = ( uintptr_t )me32.modBaseAddr;
				g_GameProcessSize = me32.modBaseSize;

				return true;
			}
		}
	   	
	}while ( Module32Next( ModuleSnap, &me32 ) );

	CloseHandle ( ModuleSnap );

	return false;
}
//=============================================================================================================
DWORD HackThread ( LPVOID lpArgs )
{
	DWORD ExitCode;

	InitializeOffsets();
	
	for ( ;; )
	{
		if ( GetGameProcess() == false || GetGameModule() == false )
		{
			BOOL bRet = GetExitCodeProcess ( GetCurrentProcess(), &ExitCode );

			if ( bRet != false )
			{
//				ExitProcess ( ExitCode );
			}	
		}
		else
		{
			UpdateCvars();
			RunHacks();
		}
	}
	
	return 0;
}
//=============================================================================================================