//=============================================================================================================
#define TYPE_BOOL ( 1 << 0 )
#define TYPE_CHAR ( 1 << 1 )
#define TYPE_SHORT ( 1 << 2 )
#define TYPE_LONG ( 1 << 3 )
#define TYPE_LONGLONG ( 1 << 4 )
#define TYPE_FLOAT ( 1 << 5 )
#define TYPE_DOUBLE ( 1 << 6 )
//=============================================================================================================
#define BOOL_MIN 0
#define BOOL_MAX 1
//=============================================================================================================
class CVar_Base
{
public:
	
	virtual char* GetIniString() = 0;
	
	virtual CVar_Base* GetParent() = 0;

	virtual double GetMinimum() = 0;
	virtual double GetMaximum() = 0;
	virtual double GetValue() = 0;
	
	virtual uintptr_t GetType() = 0;

	virtual void SetParent ( CVar_Base* Parent ) = 0;

	virtual void SetValue ( double Value ) = 0;
	
    virtual ~CVar_Base()
	{
	
	};
};
//=============================================================================================================
class CVar : public CVar_Base
{
public:

	CVar ( char* IniString, double Minimum, double Maximum, double Value, uintptr_t Type )
	{
		m_Parent = NULL;
		m_IniString = IniString;
		m_Minimum = Minimum;
		m_Maximum = Maximum;
		m_Value = Value;
		m_Type = Type;
	};

	char* GetIniString()
	{
		return m_IniString;
	}

    double GetMinimum()
	{
		return m_Minimum;
	}

	double GetMaximum()
	{
		return m_Maximum;
	}

	double GetValue()
	{
		return m_Value;
	}
	
	uintptr_t GetType()
	{
		return m_Type;
	}

	void SetValue ( double Value )
	{
		if ( Value >= m_Minimum && Value <= m_Maximum )
		{
			m_Value = Value;
		}
	}

	CVar_Base* GetParent()
	{
		return m_Parent;
	}

	void SetParent ( CVar_Base* Parent )
	{
		m_Parent = Parent;
	}
	
private:

	CVar_Base* m_Parent;
	double m_Minimum, m_Maximum, m_Value;
	char* m_IniString;
	uintptr_t m_Type;
};
//=============================================================================================================